#!/usr/bin/env bash
#
set -euo pipefail
IFS="$(printf '\n\t')"

intputfile=""
outputfile=""
counter=0
i=36788

rm -f zzufin-* zzufout-*

while true;
do
	inputfile="zzufin-${i}.tilton"
	outputfile="zzufout-${i}.result"
	errorfile="zzufout-${i}.error"

	#echo "${i}nputfile -> $outputfile"

	zzuf -s "${i}" <base02.tilton >"$inputfile"
	if ../build/tilton <"$inputfile" >"$outputfile" 2>"$errorfile";
	then
		rm -f "$inputfile" "$outputfile" "$errorfile"
	else
		echo "Keep files for test ${i}"
		((counter += 1))
	fi
	if ((counter > 3)); then
		break
	fi

	((i += 1))
done

rm -f core.tilton*

echo "$counter crashes"