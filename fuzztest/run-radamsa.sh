#!/usr/bin/env bash
#
set -euo pipefail
IFS="$(printf '\n\t')"

cmd=radamsa

intputfile=""
outputfile=""
counter=0
i=0

rm -f radamsain-* radamsaout-*

while true;
do
	inputfile="radamsain-${i}.tilton"
	outputfile="radamsaout-${i}.result"
	errorfile="radamsaout-${i}.error"

	#echo "${i}nputfile -> $outputfile"

	$cmd base03.tilton > "$inputfile"
	if ../build/tilton <"$inputfile" >"$outputfile" 2>"$errorfile";
	then
		rm -f "$inputfile" "$outputfile" "$errorfile"
	else
		echo "Keep files for test ${i}"
		((counter += 1))
	fi
	if ((counter > 2)); then
		break
	fi

	((i += 1))
done

rm -f core.tilton*

echo "$counter crashes"