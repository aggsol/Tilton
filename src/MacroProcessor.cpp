//
// Created by Kim HOANG on 22.10.2019.
//

#include "MacroProcessor.hpp"
#include "Context.hpp"
#include "node.h"
#include <string>
#include <cassert>

MacroProcessor::MacroProcessor()
{
	m_topFrame  = std::make_shared<Context>(nullptr, &m_macros);
	in_         = new Text();

	InstallTiltonMacros();
}

MacroProcessor::~MacroProcessor()
{
	m_topFrame = nullptr;
	delete in_;
	in_ = nullptr;
}

void MacroProcessor::setArgument(int pos, const std::string& value)
{
	assert(m_topFrame->GetArgument(pos)->value_ == nullptr);
	m_topFrame->GetArgument(pos)->value_ = new Text(value);
}

void MacroProcessor::setMacro(const std::string &name, const std::string &definition)
{
	assert(not name.empty());
	assert(not definition.empty());
	m_macros.InstallMacro(new Text(name), new Text(definition));
}

void MacroProcessor::InstallTiltonMacros()
{
	m_macros.setMacro("gt", ">");
	m_macros.setMacro("lt", "<");
	m_macros.setMacro("tilde", "~");
	m_macros.setMacro("tilton", "0.7");
}

void MacroProcessor::Run(bool go)
{
	// Process the input
	if (go)
	{
		// in_->ReadStdInput();
		in_->ReadFromStream(std::cin);
		in_->name("[standard input]");
		m_topFrame->ParseAndEvaluate(in_, the_output_);
	}

	// and finally
	the_output_.WriteStdOutput();
}

void MacroProcessor::Run(std::istream &input, std::ostream &output)
{
	in_->ReadFromStream(input);
	in_->name("[stream]");
	m_topFrame->ParseAndEvaluate(in_, the_output_);

	output << the_output_.str();
}
