// Copyright (c) 2011 Revelux Labs, LLC. All rights reserved.
// Use of this source code is governed by a MIT-style license that can be
// found in the LICENSE file.

#include "byte_stream.h"

#include <stdexcept>
#include <stdio.h>
#include <stdlib.h>

#include "Text.hpp"
#include "tilton.h"

ByteStream::ByteStream(Text *t)
{
	if (t == nullptr)
		throw std::runtime_error("Text t must not be nullptr");
	text_      = t; //->string_;
	line_      = 0;
	character_ = 0;
	index_     = 0;
}

ByteStream::~ByteStream() {}

int ByteStream::character() const { return character_; }

int ByteStream::index() const { return index_; }

int ByteStream::line() const { return line_; }

Text *ByteStream::text() { return text_; }

// back up one character.

int ByteStream::back()
{
	if (text_)
	{
		index_ -= 1;
		character_ -= 1;
		return text_->GetCharacter(index_);
	}
	else
	{
		return EOT;
	}
}

// return the next character.

int ByteStream::next()
{
	if (text_)
	{
		int c = text_->GetCharacter(index_);
		index_ += 1;
		character_ += 1;
		if (c == '\n' || (c == '\r' && text_->GetCharacter(index_) != '\n'))
		{
			line_ += 1;
			character_ = 0;
		}
		return c;
	}
	else
	{
		return EOT;
	}
}

// peek ahead one character

int ByteStream::peek()
{
	if (text_)
	{
		return text_->GetCharacter(index_);
	}
	else
	{
		return EOF;
	}
}
