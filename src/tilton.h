// Copyright (c) 2011 Revelux Labs, LLC. All rights reserved.
// Use of this source code is governed by a MIT-style license that can be
// found in the LICENSE file.

#ifndef SRC_TILTON_H_
#define SRC_TILTON_H_

#include <map>
#include <type_traits>
//#include "tiltonfwd.h"
// TODO: remove defines
#define EOT (-1)

class Context;
class Text;
class OptionProcessor;
class HashTable;

const int kArgZero  = 0;
const int kArgOne   = 1;
const int kArgTwo   = 2;
const int kArgThree = 3;

// Builtin is the signature for built-in functions
//typedef void (*Builtin)(Context *context, Text *&the_output);

// TODO: use int32_t
// Tilton numbers are integers.
typedef long number;

// static_assert(sizeof(number) == 4, "number type is large enough signed integer");

// kInfinity is the largest positive integer than can be held in a number.
// This value may be system dependent.

#define INFINITY ((number)0x7FFFFFFF)
const number kInfinity = 0x7FFFFFFF;

// kNAN (not a number) is the smallest integer that can be held
// in a number. On two's complement machines, it is kInfinity + 1.
// This value may be system dependent.

#define NAN ((number)0x80000000)
const number kNAN = 0x80000000;

// Unsigned ints are used in computing hash.

// TODO use uint32_t
typedef unsigned long int uint32; /* unsigned 4-byte quantities */
// TODO user uint8_t
typedef unsigned char uint8; /* unsigned 1-byte quantities */

#endif // SRC_TILTON_H_
