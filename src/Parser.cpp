//
// Created by Kim <foss@aggsol.de> on 29.11.19.
//

#include <bits/stdint-intn.h>
#include <cassert>
#include "FrameStack.hpp"
#include "HashTable.hpp"
#include "Parser.hpp"
#include "Macro.hpp"

Parser::Parser(std::ostream &output, HashTable &macros, FrameStack &frames)
	: m_output(output)
	, m_macros(macros)
	, m_frames(frames)
{
	m_frames.push();
}

Parser::~Parser()
{
	m_frames.pop();
}

void Parser::parse(std::istream &input)
{
	std::string line;
	unsigned lineCounter = 0;
	while(input)
	{
		std::getline(input, line);
		lineCounter++;

		parse(line);
	}
}

void add(FrameStack& frames, HashTable& macros, std::ostream& output)
{
	int64_t result = 0;
	auto& top = frames.top();
	for(int i=1; i<=9; ++i)
	{	
		auto& param = top.getParameter(i);
		if(param.text.empty()) continue;
		result += param.text.ConvertAlphaToInteger();
	}	
	
	output << result;
}

unsigned Parser::parse(const std::string &line)
{
	std::ostringstream buffer;
	unsigned paramIndex = 0;
	m_state = State::Start;
	unsigned col = 0;
	auto& myframe = m_frames.top();

	for(; col < line.size(); ++col)
	{
		char curr = line[col];

		if(curr == 0)
		{
			throw std::runtime_error("Invalid null (0) byte");
		}

		char peek = 0;
		if(col < line.size())
		{
			peek = line[col + 1];
		}

		switch(m_state)
		{
			case State::Start:
				if(curr == '<' && peek == '~')
				{
					myframe.clear();
					paramIndex = 0;
					col++; // consume '~'
					m_state = State::MacroStart;

				}
				else
				{
					m_output << curr;
				}
				break;
			case State::MacroStart:
				if(curr == '~' && peek == '>')
				{
					m_state = State::MacroEnd;
				}
				else if(curr == '<' && peek == '~')
				{
					assert(col > 0);
					auto subMacro = line.substr(col);
					
					buffer.clear();
					buffer.seekp(0);
					Parser parser(buffer, m_macros, m_frames);
					col += parser.parse(subMacro);					
					auto param = buffer.str();
					std::cout << "param=" << param << " paramIndex=" << paramIndex << "\n";
					myframe.setParameter(paramIndex, buffer.str());
				}
				else if(curr == '~')
				{
					paramIndex++;
				}
				else
				{
					myframe.appendToParameter(paramIndex, curr);
				}
				break;
			case State::Tilde:
				break;
			case State::MacroEnd:

				auto &name = myframe.getParameter(0).text;
				if(name.str().empty())
				{
					throw std::runtime_error("Missing name");
				}

				if(name.isDigit() && myframe.parameters().size() == 2 )
				{
					const auto& val = myframe.getParameter(1).text.str();
					std::cout << "install macro:" << name.str() << " value=" << val << "\n";
					m_macros.setMacro(name.str(), val);
				}
				else if(name.str() == "add")
				{
					add(m_frames, m_macros, m_output);
				}
				else
				{
					auto macro = m_macros.find(name);
					if(macro == nullptr)
					{
						throw std::runtime_error(name.str() + ": Unknown macro");
					}

					m_output << macro->m_definition;
				}

				if(not m_frames.isFinal()) 
				{ 
					return col;
				}
				else
				{
					m_state = State::Start;
				}

				break;
		
		}
	}

	return col;
}
