// Copyright (c) 2011 Revelux Labs, LLC. All rights reserved.
// Use of this source code is governed by a MIT-style license that can be
// found in the LICENSE file.

#include "Macro.hpp"
#include <string>
#include <cassert>
#include <cstring>

//#include "tilton.h"

Macro::Macro()
{
	init(nullptr, 0);
}

Macro::Macro(int len)
{
	init(nullptr, len);
}

Macro::Macro(const std::string& def, const std::string& name)
{
	m_definition = def;
	name_ = name;
}

Macro::Macro(Text *t)
{
	if (t)
	{
		init(t->str().c_str(), t->str().length());
	}
	else
	{
		init(nullptr, 0);
	}
}

// Macro::~Macro() {
//    delete this->m_definition;
//	this->m_definition = nullptr;
//    delete this->name_;
//	this->name_ = nullptr;
//}

void Macro::AddToString(const char *s, int len)
{
	if (s && len)
	{
		m_definition.append(s, len);
		//        CheckLengthAndIncrease(len);
		//        memmove(&m_definition[length_], s, len);
		//        length_ += len;
		//        my_hash_ = 0;
	}
}

void Macro::AddToString(Text *t)
{
	if (t)
	{
		// append(t->string_.c_str(), t->string_.length());
		m_definition += t->str();
	}
}
//
// void Macro::CheckLengthAndIncrease(int len) {
//    int newMaxLength;
//    int req = length_ + len;
//    if (max_length_ < req) {
//        newMaxLength = max_length_ * 2;
//        if (newMaxLength < req) {
//            newMaxLength = req;
//        }
//        char* newString = new char[newMaxLength];
//        memmove(newString, m_definition, max_length_);
//        delete m_definition;
//        m_definition = newString;
//        max_length_ = newMaxLength;
//    }
//}

void Macro::PrintMacroList()
{
	std::cerr << name_ << "~" << m_definition << "\n";
//	Macro *t = this;
//	while (t)
//	{
//		fwrite(t->name_.c_str(), sizeof(char), t->name_.length(), stderr);
//		if (t->m_definition.length())
//		{
//			fputc('~', stderr);
//			fwrite(t->m_definition.c_str(), sizeof(char), t->m_definition.length(), stderr);
//		}
//		fprintf(stderr, "\n");
//		t = t->link_;
//	}
}

// TODO:
int Macro::FindFirstSubstring(Text *t)
{
	if (t == nullptr)
		return -1;
	return m_definition.find(t->str());
	//  int len = t->string_.length();
	//  const char* s = t->string_.c_str();
	//  if (len) {
	//    bool b;
	//    int d = length_ - len;
	//    int i;
	//    int r;
	//    for (r = 0; r <= d; r += 1) {
	//      b = true;
	//      for (i = 0; i < len; i += 1) {
	//        if (m_definition[r + i] != s[i]) {
	//          b = false;
	//          break;
	//        }
	//      }
	//      if (b) {
	//        return r;
	//      }
	//    };
	//  }
	//  return -1;
}

void Macro::init(const char *s, int len)
{
	if(s == nullptr)
	{
		m_definition.reserve(len);
	}
	else
	{
		m_definition.assign(s, len);
	}

//	link_ = nullptr;
	//    length_ = name_length_ = 0;
	//    my_hash_ = 0;
	//    max_length_ = len;
	//    if (len == 0) {
	//        m_definition = nullptr;
	//    } else {
	//        m_definition = new char[len];
	//        if (s) {
	//            memmove(m_definition, s, len);
	//            length_ = len;
	//        }
	//    }
}

bool Macro::IsNameEqual(Text *t)
{
	if (t == nullptr)
		return false;
	return name_ == t->str();
	//    if (name_length_ != t->string_.length()) {
	//        return false;
	//    }
	//    for (int i = 0; i < name_length_; i += 1) {
	//        if (name_[i] != t->string_[i]) {
	//            return false;
	//        }
	//    }
	//    return true;
}

// TODO:
int Macro::FindLastSubstring(Text *t)
{
	if (t == nullptr)
		return -1;
	return m_definition.rfind(t->str());
	//  int len = t->string_.length();
	//  const char* s = t->string_.c_str();
	//  if (len) {
	//    bool b;
	//    int d = length_ - len;
	//    for (int r = d; r >= 0; r -= 1) {
	//      b = true;
	//      for (int i = 0; i < len; i += 1) {
	//        if (m_definition[r + i] != s[i]) {
	//          b = false;
	//          break;
	//        }
	//      }
	//      if (b) {
	//        return r;
	//      }
	//    }
	//  }
	//  return -1;
}

void Macro::set_string(Text *t)
{
	//    my_hash_ = 0;
	if (t == nullptr)
	{
		m_definition.clear();
	}
	else
	{
		m_definition = t->str();
	}
	//    if (t && t->string_.length()) {
	//        length_ = t->string_.length();
	//        if (length_ > max_length_) {
	//            delete m_definition;
	//            m_definition = new char[length_];
	//            max_length_ = length_;
	//        }
	//        memmove(m_definition, t->string_.c_str(), length_);
	//    } else {
	//        length_ = 0;
	//    }
}

void Macro::set_name(const char *s) { set_name(s, static_cast<int>(strlen(s))); }

void Macro::set_name(const char *s, int len)
{
	if (s == nullptr)
	{
		name_.reserve(len);
	}
	else
	{
		name_.assign(s, len);
	}
	//    delete name_;
	//    name_length_ = len;
	//    name_ = new char[name_length_];
	//    memmove(name_, s, name_length_);
}

void Macro::set_name(Text *t)
{
	assert(t != nullptr);
	name_ = t->str();
	//    name(t->string_.c_str(), t->string_.length());
}

void Macro::ReplaceDefWithSubstring(int start, int len)
{
	assert(start >= 0);
	assert(len >= 0);
	m_definition = m_definition.substr(start, len);
	//	memmove(m_definition, &m_definition[start], len);
	//    length_ = len;
}

//void Macro::AddToString(const std::string &str) { m_definition += str; }

void Macro::setDef(const std::string& str)
{
	m_definition= str;
}
