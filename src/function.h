// Copyright (c) 2011 Revelux Labs, LLC. All rights reserved.
// Use of this source code is governed by a MIT-style license that can be
// found in the LICENSE file.

#ifndef SRC_FUNCTION_H_
#define SRC_FUNCTION_H_

#include <map>
#include <string>
#include <cassert>

#include "MacroTable.hpp"
#include "Context.hpp"
#include "HashTable.hpp"
#include "Macro.hpp"
#include "node.h"
#include "tilton.h"

// FunctionContext -- collection of functions available as built-ins
class FunctionContext
{
  public:
	// TODO: why is this a singleton?
	// Constructor
	FunctionContext();
	virtual ~FunctionContext();

	static FunctionContext* instance();

	static std::map<std::string, Builtin> &functions()
	{
		static std::map<std::string, Builtin> functions_;
		return functions_;
	}

	Builtin GetFunction(const std::string& name)
	{
		it_ = functions().find(name);
		if (it_ != functions().end())
		{
			return it_->second;
		}
		else
		{
			return nullptr;
		}
	}

  private:

	// registerTiltonFunctions
	// Register the built-in functions for use in Tilton
	void RegisterTiltonFunctions();
	static void RegisterFunction(std::string name, Builtin function) { functions().insert(std::make_pair(name, function)); }

	static FunctionContext* pInstance;

	// Container for built ins
	//  std::map<std::string, Builtin>  functions_;
	std::map<std::string, Builtin>::const_iterator it_;
};

class ArithmeticFunction
{
  public:

	// reduce
	// Reduce a list of parameters to a single value. This is used
	// to implement the tilton arithmetic functions.
	static void reduce(ContextPtr context, number num, number (*f)(number, number), Text& the_output)
	{
		Node *n = context->first_->next();
		if (n)
		{
			if (num == kNAN)
			{
				num = context->EvaluateNumber(n, the_output);
				n   = n->next();
			}
			if (n)
			{
				for (;;)
				{
					if (!n)
					{
						break;
					}
					number d = context->EvaluateNumber(n, the_output);
					if (num == kNAN || d == kNAN)
					{
						num = kNAN;
						break;
					}
					num = f(num, d);
					n   = n->next();
				}
			}
		}
		the_output.appendNum(num);
	}

	static number add(number first, number second) { return first + second; }

	static number sub(number first, number second) { return first - second; }

	static number mult(number first, number second) { return first * second; }

	static number div(number first, number second) { return second ? first / second : kNAN; }

	static number mod(number first, number second) { return second ? first % second : kNAN; }
};

class BooleanFunction
{
  public:
//	BooleanFunction();
//	virtual ~BooleanFunction();

	//  test - This is used to implement the tilton binary conditional functions.
	static void test(ContextPtr context, int (*f)(Text *, Text *), Text& the_output)
	{
		Node *c = nullptr;
		Node *t = nullptr;
		Node *s = context->first_->next();
		if (!s)
		{
			context->ReportErrorAndDie("No parameters");
			return;
		}
		Text *swich = context->EvaluateArgument(s, the_output);
		c           = s->next();
		if (!c)
		{
			context->ReportErrorAndDie("Too few parameters");
		}
		t = c->next();
		if (!t)
		{
			context->ReportErrorAndDie("Too few parameters");
		}
		for (;;)
		{
			if (f(swich, context->EvaluateArgument(c, the_output)))
			{ // then
				the_output.append(context->EvaluateArgument(t, the_output));
				return;
			}
			c = t->next();
			if (!c)
			{
				return; // empty else
			}
			t = c->next();
			if (!t)
			{ // else
				the_output.append(context->EvaluateArgument(c, the_output));
				return;
			}
		}
	}

	static int eq(Text *first, Text *second) { return first->IsEqual(second); }

	static int ge(Text *first, Text *second) { return !(first->lt(second)); }

	static int gt(Text *first, Text *second) { return second->lt(first); }

	static int le(Text *first, Text *second) { return !(second->lt(first)); }

	static int lt(Text *first, Text *second) { return first->lt(second); }

	static int ne(Text *first, Text *second) { return !(first->IsEqual(second)); }
};

// AddFunction -- Function object for built-in function
class AddFunction : public ArithmeticFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		ArithmeticFunction::reduce(context, 0, ArithmeticFunction::add, the_output);
	}
};

// AndFunction -- Function object for built-in function
class AndFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		Node *n = context->first_->next();
		Text *t = nullptr;
		while (n)
		{
			t = context->EvaluateArgument(n, the_output);
			if (t->empty())
			{
				return;
			}
			n = n->next();
		}
		the_output.append(t);
	}
};

// AppendFunction -- Function object for built-in function
class AppendFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		Node *n = context->first_->next();
		if(n == nullptr)
		{
			context->ReportErrorAndDie("Missing name");
		}
		Text *name = context->EvaluateArgument(n, the_output);
		if(name == nullptr || name->empty())
		{
			context->ReportErrorAndDie("Missing name");
		}

//		Macro *t = context->macros()->GetMacroDefOrInsertNull(name);
		auto t = context->macros()->GetMacroDefOrInsertNull(name);

		for (;;)
		{
			n = n->next();
			if (!n)
			{
				break;
			}
			t->AddToString(context->EvaluateArgument(n, the_output));
		}
	}
};

// DefineFunction -- Function object for built-in function
class DefineFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		int position = the_output.str().length();
		the_output.append(context->GetArgument(kArgTwo)->text_);
		Text *name = context->EvaluateArgument(kArgOne, the_output);
		if(name == nullptr || name->empty())
		{
			context->ReportErrorAndDie("Missing name");
		}
		context->macros()->InstallMacro(name, the_output.RemoveFromString(position));
	}
};

// DefinedFunction -- Function object for built-in function
class DefinedFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		auto name = context->EvaluateArgument(kArgOne, the_output);

		if(name == nullptr)
		{
			context->ReportErrorAndDie("Missing name");
		}

		the_output.append(context->EvaluateArgument(
				context->macros()->LookupMacro(name)
				? kArgTwo
				: kArgThree,
				the_output));
	}
};

// DeleteFunction -- Function object for built-in function
class DeleteFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		Node *n = context->first_->next();
		while (n)
		{
			Text*  name = context->EvaluateArgument(n, the_output);
			assert(name != nullptr);
			context->macros()->deleteMacro(name->str());
			n = n->next();
		}
	}
};

// DivFunction -- Function object for built-in function
class DivFunction : public ArithmeticFunction
{
  public:
//	DivFunction();
//	virtual ~DivFunction();

	static void evaluate(ContextPtr context, Text& the_output)
	{
		ArithmeticFunction::reduce(context, kNAN, ArithmeticFunction::div, the_output);
	}
};

// DumpFunction -- Function object for built-in function
class DumpFunction
{
  public:
	static void evaluate(ContextPtr context, Text&)
	{
		context->macros()->PrintMacroTable();
	}
};

// EntityifyFunction -- Function object for built-in function
class EntityifyFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		Text *t = context->EvaluateArgument(kArgOne, the_output);
		int   c;
		//int   i;
		if (t && t->str().length())
		{
			for (size_t i = 0; i < t->str().length(); i += 1)
			{
				c = t->GetCharacter(i);
				switch (c)
				{
				case '&':
					the_output.append("&amp;");
					break;
				case '<':
					the_output.append("&lt;");
					break;
				case '>':
					the_output.append("&gt;");
					break;
				case '"':
					the_output.append("&quot;");
					break;
				case '\'':
					the_output.append("&#039;");
					break;
				case '\\':
					the_output.append("&#092;");
					break;
				case '~':
					the_output.append("&#126;");
					break;
				default:
					the_output.append(c);
				}
			}
		}
	}
};

// EqFunction -- Function object for built-in function
class EqFunction
{
  public:
//	EqFunction();
//	virtual ~EqFunction();

	static void evaluate(ContextPtr context, Text& the_output)
	{
		BooleanFunction::test(context, BooleanFunction::eq, the_output);
	}
};

// EvalFunction -- Function object for built-in function
class EvalFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		auto new_context = std::make_shared<Context>(context);
		new_context->AddArgument("eval");
		new_context->AddArgument("<~2~>");
		new_context->AddArgument("<~3~>");
		new_context->AddArgument("<~4~>");
		new_context->AddArgument("<~5~>");
		new_context->AddArgument("<~6~>");
		new_context->AddArgument("<~7~>");
		new_context->AddArgument("<~8~>");
		new_context->ParseAndEvaluate(context->GetArgument(kArgOne)->text_, the_output);
	}
};

// FirstFunction -- Function object for built-in function
class FirstFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		Node *arg  = context->first_->next();
		Text *name = context->EvaluateArgument(arg, the_output);
		if(name == nullptr || name->empty())
		{
			context->ReportErrorAndDie("Missing name: first");
		}
		Macro *macro = context->macros()->LookupMacro(name);
		if (!macro)
		{
			context->ReportErrorAndDie("Undefined variable", name);
			return;
		}
		Text *d   = nullptr;
		int   len = 0;
		int   r   = macro->m_definition.length();
		for (;;)
		{
			arg = arg->next();
			if (!arg)
			{
				break;
			}
			int index = macro->FindFirstSubstring(context->EvaluateArgument(arg, the_output));
			if (index >= 0 && index < r)
			{
				r   = index;
				d   = context->EvaluateArgument(arg, the_output);
				len = d->str().length();
			}
		}
		the_output.append(macro->m_definition.substr(0, r));
		macro->ReplaceDefWithSubstring(r + len, macro->m_definition.length() - (r + len));
		arg = context->prev()->GetArgument(kArgZero);
		delete arg->text_;
		arg->text_ = nullptr;
		delete arg->value_;
		arg->value_ = d ? new Text(d) : nullptr;
	}
};

// GeFunction -- Function object for built-in function
class GeFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		BooleanFunction::test(context, BooleanFunction::ge, the_output);
	}
};

// GensymFunction -- Function object for built-in function
class GensymFunction
{
  public:
	static void evaluate(ContextPtr , Text& the_output)
	{
		static number theSequenceNumber = 1000;
		theSequenceNumber += 1;
		the_output.appendNum(theSequenceNumber);
	}
};

// GetFunction -- Function object for built-in function
class GetFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		Node *n = context->first_->next();
		while (n)
		{
			Text * name  = context->EvaluateArgument(n, the_output);
			Macro *macro = context->macros()->LookupMacro(name);
			if (macro)
			{
				the_output.append(new Text(macro));
			}
			else
			{
				context->ReportErrorAndDie("Undefined variable", name);
			}
			n = n->next();
		}
	}
};

// GtFunction -- Function object for built-in function
class GtFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		BooleanFunction::test(context, BooleanFunction::gt, the_output);
	}
};

// IncludeFunction -- Function object for built-in function
class IncludeFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		// Node* n = context->first_->next();
		Text *string = new Text();
		Text *name   = context->EvaluateArgument(kArgOne, the_output);
		if (!string->ReadFromFile(name))
		{
			context->ReportErrorAndDie("Error in reading file", name);
		}
		auto new_context = std::make_shared<Context>(context);

		new_context->AddArgument("include");
		new_context->AddArgument("<~2~>");
		new_context->AddArgument("<~3~>");
		new_context->AddArgument("<~4~>");
		new_context->AddArgument("<~5~>");
		new_context->AddArgument("<~6~>");
		new_context->AddArgument("<~7~>");
		new_context->AddArgument("<~8~>");
		new_context->ParseAndEvaluate(string, the_output);

		delete string;
	}
};

// LastFunction -- Function object for built-in function
class LastFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		Node *n    = context->first_->next();
		Text *name = context->EvaluateArgument(kArgOne, the_output);
		if(name == nullptr || name->empty())
		{
			context->ReportErrorAndDie("Missing name");
		}
		Macro *macro = context->macros()->LookupMacro(name);
		if (!macro)
		{
			context->ReportErrorAndDie("Undefined variable", name);
			return;
		}
		Text *d   = nullptr;
		int   len = 0;
		int   r   = 0;
		for (;;)
		{
			n = n->next();
			if (!n)
			{
				break;
			}
			int index = macro->FindLastSubstring(context->EvaluateArgument(n, the_output));
			if (index > r)
			{
				r   = index;
				d   = context->EvaluateArgument(n, the_output);
				len = d->str().length();
			}
		}
		the_output.append(macro->m_definition.substr(r + len));
//		the_output.append(macro->m_definition.c_str() + r + len, macro->m_definition.length() - (r + len));
		// TODO: is this a resize?
		//    macro->length_ = r;
		macro->m_definition.resize(r, '@');
		n = context->prev()->GetArgument(kArgZero);
		delete n->text_;
		n->text_ = nullptr;
		delete n->value_;
		n->value_ = d ? new Text(d) : nullptr;
	}
};

// LeFunction -- Function object for built-in function
class LeFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		BooleanFunction::test(context, BooleanFunction::le, the_output);
	}
};

// LengthFunction -- Function object for built-in function
class LengthFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		the_output.appendNum(context->EvaluateArgument(kArgOne, the_output)->utfLength());
	}
};

// LiteralFunction -- Function object for built-in function
class LiteralFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		the_output.append(context->GetArgument(kArgOne)->text_);
	}
};

// LoopFunction -- Function object for built-in function
class LoopFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		// Node* n = context->first_->next();
		auto counter = context->EvaluateArgument(kArgOne, the_output);
		while (counter->str().length() > 0)
		{
			context->ResetArgument(kArgOne);
			context->ResetArgument(kArgTwo);
			the_output.append(context->EvaluateArgument(kArgTwo, the_output));
			counter = context->EvaluateArgument(kArgOne, the_output);
		}
	}
};

// LtFunction -- Function object for built-in function
class LtFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		BooleanFunction::test(context, BooleanFunction::lt, the_output);
	}
};

// ModFunction -- Function object for built-in function
class ModFunction : public ArithmeticFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		ArithmeticFunction::reduce(context, kNAN, ArithmeticFunction::div, the_output);
	}
};

// MultFunction -- Function object for built-in function
class MultFunction : public ArithmeticFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		ArithmeticFunction::reduce(context, 1, ArithmeticFunction::mult, the_output);
	}
};

// MuteFunction -- Function object for built-in function
//class MuteFunction
//{
//  public:
////	MuteFunction();
////	virtual ~MuteFunction();
//
//	static void evaluate(ContextPtr context, Text& the_output)
//	{
//		Node *n = context->first_->next();
//		while (n)
//		{
//			context->EvaluateArgument(n, the_output);
//			n = n->next();
//		}
//	}
//};

// NeFunction -- Function object for built-in function
class NeFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		BooleanFunction::test(context, BooleanFunction::ne, the_output);
	}
};

// NullFunction -- Function object for built-in function
class NullFunction
{
  public:
	static void evaluate(ContextPtr , Text&) {}
};

// NumberFunction -- Function object for built-in function
class NumberFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		number num = context->EvaluateArgument(kArgOne, the_output)->getNumber();
		the_output.append(context->EvaluateArgument(num != kNAN ? 2 : 3, the_output));
	}
};

// OrFunction -- Function object for built-in function
class OrFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		Node *n = context->first_->next();
		Text *t = nullptr;
		while (n)
		{
			t = context->EvaluateArgument(n, the_output);
			if (t->str().length())
			{
				break;
			}
			n = n->next();
		}
		the_output.append(t);
	}
};

// PrintFunction -- Function object for built-in function
class PrintFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		Text *value = context->EvaluateArgument(kArgOne, the_output);
		if(value != nullptr)
		{
			std::cerr << value->str() << std::endl;
		}
	}
};

// ReadFunction -- Function object for built-in function
class ReadFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		Text *string = new Text();
		Text *name   = context->EvaluateArgument(kArgOne, the_output);
		if (!string->ReadFromFile(name))
		{
			context->ReportErrorAndDie("Error in reading file", name);
		}
		the_output.append(string);
		delete string;
	}
};

// RepFunction -- Function object for built-in function
class RepFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		// Node* n = context->first_->next();
		Text *value = context->EvaluateArgument(kArgOne, the_output);
		for (number num = context->EvaluateNumber(kArgTwo, the_output); num > 0; num -= 1)
		{
			the_output.append(value);
		}
	}
};

// SetFunction -- Function object for built-in function
class SetFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		auto *name = context->EvaluateArgument(kArgOne, the_output);
		if(name == nullptr || name->empty())
		{
			context->ReportErrorAndDie("Missing name");
		}
		context->macros()->InstallMacro(name, context->EvaluateArgument(kArgTwo, the_output));
	}
};

// SlashifyFunction -- Function object for built-in function
class SlashifyFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		Text *t = context->EvaluateArgument(kArgOne, the_output);
		int   c;
		//int   i;
		if (t && t->str().length())
		{
			for (size_t i = 0; i < t->str().length(); i += 1)
			{
				c = t->GetCharacter(i);
				switch (c)
				{
				case '\\': // backslash
				case '\'': // single quote
				case '"':  // double quote
					the_output.append('\\');
					break;
				}
				the_output.append(c);
			}
		}
	}
};

// StopFunction -- Function object for built-in function
class StopFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		context->ReportErrorAndDie("Stop", context->EvaluateArgument(kArgOne, the_output));
	}
};

// SubFunction -- Function object for built-in function
class SubFunction : public ArithmeticFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		ArithmeticFunction::reduce(context, kNAN, ArithmeticFunction::sub, the_output);
	}
};

// SubstrFunction -- Function object for built-in function
class SubstrFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		Node *arg    = context->first_->next();
		Text *string = context->EvaluateArgument(arg, the_output);
		arg          = arg->next();
		if (arg)
		{
			number start = context->EvaluateNumber(arg, the_output);
			if (start < 0)
			{
				start += string->str().length();
			}
			number len = kInfinity;
			arg        = arg->next();
			if (arg)
			{
				len = context->EvaluateNumber(arg, the_output);
			}
			if (start >= 0 && len > 0)
			{
				the_output.append(context->EvaluateArgument(kArgOne, the_output)
										   ->utfSubstr(static_cast<int>(start), static_cast<int>(len)));
			}
		}
	}
};

// TrimFunction -- Function object for built-in function
class TrimFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		Node *n = context->first_->next();
		while (n)
		{
			the_output.RemoveSpacesAddToString(context->EvaluateArgument(n, the_output));
			n = n->next();
		}
	}
};

// UnicodeFunction -- Function object for built-in function
class UnicodeFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		Node *n = context->first_->next();
		while (n)
		{
			number num = context->EvaluateNumber(n, the_output);
			if (num >= 0)
			{
				int i = static_cast<int>(num);
				if (i <= 0x7F)
				{
					the_output.append(i);
				}
				else if (i <= 0x7FF)
				{
					the_output.append(0xC000 | (i >> 6));
					the_output.append(0x8000 | (i & 0x3F));
				}
				else if (i <= 0xFFFF)
				{
					the_output.append(0xE000 | (i >> 12));
					the_output.append(0x8000 | ((i >> 6) & 0x3F));
					the_output.append(0x8000 | (i & 0x3F));
				}
				else
				{
					the_output.append(0xF000 | (i >> 18));
					the_output.append(0x8000 | ((i >> 12) & 0x3F));
					the_output.append(0x8000 | ((i >> 6) & 0x3F));
					the_output.append(0x8000 | (i & 0x3F));
				}
			}
			else
			{
				context->ReportErrorAndDie("Bad character code", context->EvaluateArgument(n, the_output));
				return;
			}
			n = n->next();
		}
	}
};

// WriteFunction -- Function object for built-in function
class WriteFunction
{
  public:
	static void evaluate(ContextPtr context, Text& the_output)
	{
		Text *name = context->EvaluateArgument(kArgOne, the_output);
		if (!context->EvaluateArgument(kArgTwo, the_output)->WriteToFile(name))
		{
			context->ReportErrorAndDie("Error in writing file", name);
		}
	}
};
#endif // SRC_FUNCTION_H_
