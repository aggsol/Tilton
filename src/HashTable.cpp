// Copyright (c) 2011 Revelux Labs, LLC. All rights reserved.
// Use of this source code is governed by a MIT-style license that can be
// found in the LICENSE file.

#include "HashTable.hpp"
#include "Macro.hpp"
#include "Text.hpp"
#include <cassert>

void HashTable::setMacro(const std::string &name, const std::string &def)
{
	auto it = m_hashTable.find(name);
	if(it != m_hashTable.end())
	{
		it->second->setDef(def);
	}
	else
	{
		m_hashTable[name] = new Macro(def, name);
	}
}

Macro *HashTable::LookupMacro(Text *name)
{
	assert(name != nullptr);
	auto it = m_hashTable.find(name->str());
	if(it != m_hashTable.end())
	{
		assert(it->second != nullptr);
		return it->second;
	}
	return nullptr;
}

Macro *HashTable::find(Text &name)
{
	auto it = m_hashTable.find(name.str());
	if(it != m_hashTable.end())
	{
		assert(it->second != nullptr);
		return it->second;
	}
	return nullptr;
}

void HashTable::InstallMacro(Text *name, Text *value)
{
	assert(name != nullptr);
	if(value == nullptr)
	{
		setMacro(name->str(), "");
	}
	else
	{
		setMacro(name->str(), value->str());
	}
}

void HashTable::InstallMacro(Text *name, Macro *value)
{
	assert(name != nullptr);
	assert(value != nullptr);
	auto it = m_hashTable.find(name->str());
	if(it != m_hashTable.end())
	{
		delete it->second;
	}
	value->name_ = name->str();
	m_hashTable[name->str()] = value;
}

void HashTable::PrintMacroTable()
{
	for(auto& pair: m_hashTable)
	{
		pair.second->PrintMacroList();
	}
}

Macro *HashTable::GetMacroDefOrInsertNull(Text *name)
{
	auto it = m_hashTable.find(name->str());
	if(it != m_hashTable.end())
	{
		return it->second;
	}

	auto pMacro = new Macro("", name->str());
	m_hashTable[name->str()] = pMacro;
	return pMacro;
}

void HashTable::deleteMacro(const std::string& name)
{
	auto it = m_hashTable.find(name);
	if(it == m_hashTable.end()) return;

	it->second->name_ = "deleted";
	it->second->m_definition = "~deleted~";
	delete it->second;
	m_hashTable.erase(name);
}

