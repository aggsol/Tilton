//
// Created by kim on 19.11.19.
//

#ifndef TILTON_BUILTINS_HPP
#define TILTON_BUILTINS_HPP

#include "Context.hpp"

class Text;

class BuiltIns
{
public:
	static void add(ContextPtr ctx, Text &output);
};


#endif //TILTON_BUILTINS_HPP
