//
// Created by Kim <foss@aggsol.de> on 02.12.19.
//

#include "Frame.hpp"

Parameter &Frame::getParameter(unsigned index)
{
	if(index > 9)
	{ 
		throw std::runtime_error("Parameter index must be [0,9]"); 
	}
	while(m_parameters.size() <= index)
	{
		m_parameters.emplace_back();
	}
	return m_parameters.at(index);
}

const Parameter &Frame::getParameter(unsigned index) const
{
	if(index > 9)
	{ 
		throw std::runtime_error("Parameter index must be [0,9]"); 
	}
	return m_parameters.at(index);
}

void Frame::clear()
{
	m_parameters.clear();
}

void Frame::setParameter(unsigned index, const std::string& param)
{
	getParameter(index).text.str(param);
}

void Frame::appendToParameter(unsigned index, char c)
{
	getParameter(index).text.append(c);
}
