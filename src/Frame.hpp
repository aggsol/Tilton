//
// Created by kim on 02.12.19.
//

#ifndef TILTON_FRAME_HPP
#define TILTON_FRAME_HPP

#include "Parameter.h"
#include <map>
#include <vector>

class Frame
{
public:
	Frame() = default;

	const std::vector<Parameter>& parameters() const { return m_parameters; }

	Parameter &getParameter(unsigned index);
	const Parameter &getParameter(unsigned index) const;

	void setParameter(unsigned index, const std::string& param);
	void appendToParameter(unsigned index, char c);
	void clear();

	[[nodiscard]]
	bool empty() const
	{
		return m_parameters.empty();
	}

private:
	std::vector<Parameter> m_parameters;
};


#endif //TILTON_FRAME_HPP
