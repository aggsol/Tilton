//
// Created by kim on 28.11.19.
//

#ifndef TILTON_PARAMETER_H
#define TILTON_PARAMETER_H

#include "Text.hpp"

class Parameter
{
public:
	Parameter() = default;

	Parameter(const Parameter &p) = default;

	explicit Parameter(const Text &txt);

	Text text;
	Text value;
};


#endif //TILTON_PARAMETER_H
