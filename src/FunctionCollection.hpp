//
// Created by Kim HOANG on 12.11.2019.
//
#pragma once

#include <functional>
#include <map>
#include <string>

template<typename Functor>
class FunctionCollection
{
public:
	FunctionCollection() = default;

	void		add(const std::string& name, Functor fun) { m_functions[name] = fun; };

	Functor&	get(const std::string& name)
	{ 
		return m_functions.at(name); 
	}

	Functor*	find(const std::string& name)
	{
		auto it =  m_functions.find(name);
		if(it == m_functions.end()) return nullptr;
		return &m_functions.at(name);
	}

private:
	std::map<std::string, Functor>	m_functions;
};
