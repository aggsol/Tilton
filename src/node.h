// Copyright (c) 2011 Revelux Labs, LLC. All rights reserved.
// Use of this source code is governed by a MIT-style license that can be
// found in the LICENSE file.

#ifndef SRC_NODE_H_
#define SRC_NODE_H_

#include "Text.hpp"
#include <cassert>

class Text;

// Node -- represents items in a simple linked lists.

class Node
{
  public:
	explicit Node(Text *t);
	virtual ~Node();

	Node* next() { return next_; }
	void next(Node* n) { assert(n != nullptr); next_ = n; }

	// WriteNode
	// Recursively visit each node on the list and print the text
	void WriteNode();

	Text *text_  = nullptr;
	Text *value_ = nullptr;

	bool hasValue() { return this->value_ != nullptr; }

private:
	Node *next_  = nullptr;

};

#endif // SRC_NODE_H_
