// Copyright (c) 2011 Revelux Labs, LLC. All rights reserved.
// Use of this source code is governed by a MIT-style license that can be
// found in the LICENSE file.

#include "Context.hpp"
#include <memory>
#include <stdexcept>
#include "byte_stream.h"
#include "function.h"
#include "node.h"
#include "Text.hpp"
#include "tilton.h"
#include <cassert>

Context::Context(ContextPtr prev)
: previous_(prev)
{
	assert(prev != nullptr);
	source_ = prev->source_;
	m_macros = prev->m_macros;

	if (source_)
	{
		line_      = source_->line();
		character_ = source_->character();
		index_     = source_->index();
	}

}

Context::Context(ByteStream *s, HashTable* macros)
: source_(s)
, m_macros(macros)
{
	if (s)
	{
		line_      = s->line();
		character_ = s->character();
		index_     = s->index();
	}
}

Context::~Context()
{
	delete this->first_;
	this->first_ = nullptr;
	last_        = nullptr;
	source_      = nullptr;
}

void Context::AddArgument(const char *s) { AddArgument(new Text(s)); }

void Context::AddArgument(Text *t)
{
	Node *p = new Node(t);
	if (last_)
	{
		last_->next(p);
	}
	else
	{
		first_ = p;
	}
	last_ = p;
}

void Context::ReportErrorAndDie(const char *reason) { ReportErrorAndDie(reason, nullptr); }

void Context::ReportErrorAndDie(const char *reason, Text *evidence)
{
	Text *report = new Text(80);
	FindError(report);
	report->append(reason);
	if (evidence)
	{
		report->append(": ");
		report->append(evidence);
	}
	report->append(".\n");
	std::cerr << report->str();
	throw std::runtime_error("Abort");
}

//void Context::ParseAndEvaluate(Text *input, Text *&the_output)
//{
//	int c = 0;               // current character
//	int      depth       = 0; // depth of nested <~ ~>
//	int      tildes_seen = 0; // the number of tildes in the separator ~~~
//	ContextPtr	new_context;
//	auto in     = new ByteStream(input);
//
//	// Loop over the characters in the input
//
//	for (;;)
//	{
//		switch ((c = in->next()))
//		{
//		case '<':
//			this->ParseLeftAngle(in, depth, the_output, tildes_seen, new_context);
//			break;
//		case '~':
//			this->ParseTilde(in, depth, the_output, tildes_seen, new_context);
//			break;
//		case EOT:
//			this->ParseEOT(in, depth, the_output, tildes_seen, new_context);
//			return;
//		// literal character
//		case 0:
//			throw std::runtime_error("Invalid null byte");
//		default:
//			the_output->append(c);
//			break;
//		}
//	}
//}

void Context::ParseAndEvaluate(Text* input, Text& the_output)
{
	int c = 0;               // current character
	int      depth       = 0; // depth of nested <~ ~>
	int      tildes_seen = 0; // the number of tildes in the separator ~~~
	ContextPtr	new_context;
	ByteStream in(input);

	// Loop over the characters in the input

	for (;;)
	{
		switch ((c = in.next()))
		{
			case '<':
				ParseLeftAngle(in, depth, the_output, tildes_seen, new_context);
				break;
			case '~':
				ParseTilde(in, depth, the_output, tildes_seen, new_context);
				break;
			case EOT:
				ParseEOT(in, depth, the_output, tildes_seen, new_context);
				return;
				// literal character
			case 0:
				throw std::runtime_error("Invalid null byte");
			default:
				the_output.append(c);
				break;
		}
	}
}

void Context::ParseLeftAngle(ByteStream& in, int &depth, Text& the_output, int &tildes_seen, ContextPtr& new_context)
{
	int run_length; // the number of tildes currently under consideration

	run_length = checkForTilde(in, 0);

	if (!stackEmpty(depth))
	{ // in middle of macro expansion
		if (haveTildes(run_length))
		{ // found embedded macro bracket
			depth += 1;
		}
		the_output.append('<');
		the_output.append('~', run_length);
	}
	else if (haveTildes(run_length))
	{ // found first macro bracket
		depth                  = 1;
		tildes_seen            = run_length;
		std::shared_ptr<Context> self = shared_from_this();
		new_context            = std::make_shared<Context>(self);
		new_context->position_ = the_output.str().length();
	}
	else
	{ // left angle in the middle of text being passed through
		the_output.append('<');
	}
}

void Context::ParseTilde(ByteStream& in, int &depth, Text& the_output, int &tildes_seen, ContextPtr& new_context)
{
	int   arg_number = -1; // argument number
	Node *arg = nullptr;        // current arg
	Text *arg_text = nullptr;   // current text
	int   run_length = -1; // the number of tildes currently under consideration

	run_length = checkForTilde(in, 1);

	// if in middle of expansion and tildes seen, create args for each
	if (depth == 1 && run_length >= tildes_seen)
	{
		new_context->AddArgument(the_output.RemoveFromString(new_context->position_));
		for (;;)
		{
			run_length -= tildes_seen;
			if (run_length < tildes_seen)
			{
				break;
			}
			new_context->AddArgument(new Text());
		}
	}

	// pass through tildes in plain text
	the_output.append('~', run_length);

	// if we have seen the closing bracket
	if (in.peek() == '>')
	{
		//  ~>
		in.next();
		if (!stackEmpty(depth))
		{
			depth -= 1;
			if (!stackEmpty(depth))
			{
				the_output.append('>');
			}
			else
			{
				if (run_length)
				{
					new_context->ReportErrorAndDie("Short ~>");
				}
				//    apply:
				arg = new_context->first_;
				//    <~NUMBER~>
				arg_text   = arg->text_;
				arg_number = arg_text->GetCharacter(0) - '0';
				if (Text::isDigit(arg_number))
				{
					arg_number = arg_text->ConvertAlphaToInteger();
					if (arg_number >= 0)
					{
						if (!arg->next())
						{
							the_output.append(EvaluateArgument(arg_number, the_output));
						}
						else
						{
							//    <~NUMBER~value~>
							arg = new_context->GetArgument(arg_number);
							if (!arg->hasValue())
							{
								arg = EvalTextForArg(1, new_context, the_output);
							}
							this->SetMacroVariable(arg_number, arg->value_);
						}
//						delete new_context;
						new_context = nullptr;
						return;
					}
				}
				//    look up
				Context::EvaluateMacro(new_context, the_output);
			}
		}
		else
		{
			auto self = shared_from_this();
			auto newCtx = std::make_shared<Context>(self);
			newCtx->ReportErrorAndDie("Extra ~>");
		}
	}
}

Node *Context::EvalTextForArg(int arg_number, ContextPtr& new_context, Text& the_output)
{
	Node *n = new_context->GetArgument(arg_number);
	this->ParseAndEvaluate(n->text_, the_output);
	n->value_ = the_output.RemoveFromString(new_context->position_);
	return n;
}

void Context::SetMacroVariable(int varNo, Text *t)
{
	Node *o = this->GetArgument(varNo);
	delete o->text_;
	o->text_ = nullptr;
	delete o->value_;
	// o->value[varNo] = new Text(t);
	// previous bug fix -- jr 19Sep11
	o->value_ = new Text(t);
}

void Context::EvaluateMacro(ContextPtr& new_context, Text& the_output)
{
	auto name = new_context->EvaluateArgument(kArgZero, the_output);

	auto func = m_functions.find(name->str());
	if(func != nullptr)
	{
		(*func)(new_context, the_output);
	}
	else
	{
		// look for name as built in
		auto function = FunctionContext::instance()->GetFunction(name->str());
		if(function)
		{
			(*function)(new_context, the_output);
		}
		else
		{
			// look for macro definition
			auto macro = m_macros->LookupMacro(name);
			if(macro)
			{
				new_context->ParseAndEvaluate(new Text(macro), the_output);
			}
			else
			{
				//    undefined
				new_context->ReportErrorAndDie("Undefined macro");
			}
		}
	}
//	delete new_context;
	new_context = nullptr;
}

void Context::ParseEOT(ByteStream&, int &depth, Text&, int &, ContextPtr& new_context)
{
	if (!stackEmpty(depth))
	{
		new_context->ReportErrorAndDie("Missing ~>");
	}
}

Text *Context::EvaluateArgument(int argNr, Text& the_output)
{
	return EvaluateArgument(GetArgument(argNr), the_output);
}

Text *Context::EvaluateArgument(Node *n, Text& the_output)
{
	if (n == nullptr)
	{
		return nullptr;
	}
	if (n->value_ == nullptr)
	{
		if (n->text_ == nullptr)
		{
			return nullptr;
		}
		Text *arg      = n->text_;
		int   position = the_output.str().length();
		this->previous_->ParseAndEvaluate(arg, the_output);
		n->value_ = the_output.RemoveFromString(position);
	}
	return n->value_;
}

Text *Context::EvaluateArgument(Node& n, Text& the_output)
{
	if (n.value_ == nullptr)
	{
		if (n.text_ == nullptr)
		{
			return nullptr;
		}
		Text *arg      = n.text_;
		int   position = the_output.str().length();
		this->previous_->ParseAndEvaluate(arg, the_output);
		n.value_ = the_output.RemoveFromString(position);
	}
	return n.value_;
}

number Context::EvaluateNumber(int argNr, Text& the_output) { return EvaluateNumber(GetArgument(argNr), the_output); }

number Context::EvaluateNumber(Node *n, Text& the_output)
{
	if (!n)
	{
		return 0;
	}
	number num = EvaluateArgument(n, the_output)->getNumber();
	if (num == kNAN)
	{
		ReportErrorAndDie("Not a number", EvaluateArgument(n, the_output));
	}
	return num;
}


Node *Context::GetArgument(int argNr)
{
	Node *p = nullptr;
	if (!first_)
	{
		first_ = last_ = new Node(nullptr);
	}
	p = first_;
	for (;;)
	{
		if (!argNr)
		{
			return p;
		}
		p = p->next();
		if (!p)
		{
			p            = new Node(nullptr);
			last_->next(p); // fixed <~1~> bug -- jr 17Aug11
			last_        = p;
		}
		argNr -= 1;
	}
}

void Context::ResetArgument(int argNr)
{
	Node *n;
	n = this->GetArgument(argNr);
	delete n->value_;
	n->value_ = nullptr;
}

void Context::FindError(Text *report)
{
	if (previous_)
	{
		previous_->FindError(report);
	}
	if (source_)
	{
		report->append(source_->text()->name());
		report->append("(line:");
		report->appendNum(line_ + 1);
		report->append(", col:");
		report->appendNum(character_ + 1);
		report->append("/char:");
		report->appendNum(index_ + 1);
		report->append(") ");
	}
	// add first_ jr 4Sep11
	if (first_ && first_->text_ && first_->text_->str().length())
	{
		report->append("<~");
		report->append(first_->text_);
		report->append("~> ");
	}
}

int Context::checkForTilde(ByteStream& in, int no)
{
	while (in.next() == '~')
	{
		no += 1;
	}
	in.back();
	return no;
}

ContextPtr Context::prev()
{
	return previous_;
}

ContextPtr Context::self()
{
	return shared_from_this();
}

