// Copyright (c) 2011 Revelux Labs, LLC. All rights reserved.
// Copyright (C) 2019 Kim HOANG <foss@aggsol.de>
// Use of this source code is governed by a MIT-style license that can be
// found in the LICENSE file.

#include "MacroProcessor.hpp"
#include "function.h"
#include "CliArguments.hpp"
#include <stdexcept>

void printUsage()
{
	std::cout  << R"(
Usage: tilton [OPTIONS] [PARAMS] <input.file >output.file
Process macros in input.file (stdin) and write to output file (stdout)
Example: tilton Carly Hollywood <template.tilton >index.html
PARAMS are set as numeric macro arguments in order from 1-9

Options:
  -s, --set KEY VALUE    Set the variable KEY to VALUE.
                         Read the VALUE with '<~KEY~>' 

Flags:
  --version              Print version number
  -h, --help             Show this help

)";
}

int main(int argc, const char *argv[])
{
	bodhi::CliArguments args(argc, argv);

   	auto name = args.getOpt<std::string>("n", "name", "none");

   	auto version = args.getOpt("", "version");
   	auto help = args.getOpt("h", "help");
	
	if(help)
	{
		printUsage();
		return 0;
	}

	if(version)
	{
		std::cout << "0.7" << std::endl;
		return 0;
	}

	try
	{
		MacroProcessor tilton_processor;

		const std::string NONE = "<~~>";

		auto setVar = args.getOpt<std::string>("s", "set", NONE);
		while(setVar != NONE)
		{
			if(args.arguments().empty())
			{
				throw std::runtime_error("Missing value for option -s/--set var=" + setVar);
			}

			auto value = args.arguments().front();
			args.arguments().pop_front();

			if(value.empty())
			{
				throw std::runtime_error("Empty value for option -s/--set var=" + setVar);
			}

			tilton_processor.setMacro(setVar, value);

			setVar = args.getOpt<std::string>("s", "set", NONE);
		}

		for(size_t i=0; i<args.arguments().size(); ++i)
		{
			if(i == 9) break;
			tilton_processor.setArgument(i+1, args.arguments().at(i));
		}

		tilton_processor.Run(true);
	}
	catch (const std::runtime_error& runError)
	{
		std::cerr << runError.what() << std::endl;
		return 0;
	}
	return 0;
}
