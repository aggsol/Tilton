cmake_minimum_required(VERSION 3.14)

# library
add_library(
		libtilton
		STATIC
		byte_stream.cpp
		Context.cpp
		function.cpp
		HashTable.cpp
		Macro.cpp
		node.cpp
		option.cpp
		node.cpp
		option.cpp
		Text.cpp
		MacroProcessor.hpp
		MacroProcessor.cpp
		MacroTable.cpp
		MacroTable.hpp
		BuiltIns.cpp
		BuiltIns.hpp
		Parameter.cpp
		Parameter.h
		Parser.cpp 
		Parser.hpp
		Tokens.h
		Frame.cpp
		Frame.hpp
		FrameStack.cpp
		FrameStack.hpp
)

target_compile_definitions(libtilton PRIVATE _FORTIFY_SOURCE=2)
target_compile_features(libtilton PRIVATE cxx_std_17)
target_compile_options(libtilton
    PRIVATE
    -Wall -Wextra
    -Wshadow -Wnon-virtual-dtor -Wold-style-cast
    -Woverloaded-virtual -Wzero-as-null-pointer-constant
    -pedantic -fno-rtti
)

# program
add_executable(tilton
    tilton.cpp
)

target_link_libraries(tilton libtilton)
target_compile_features(tilton PRIVATE cxx_std_17)
target_compile_definitions(tilton PRIVATE _FORTIFY_SOURCE=2)
target_compile_options(tilton
	PRIVATE
    -Wall -Wextra
	-Wshadow -Wnon-virtual-dtor -Wold-style-cast
	-Woverloaded-virtual -Wzero-as-null-pointer-constant
    -pedantic -fno-rtti
)
