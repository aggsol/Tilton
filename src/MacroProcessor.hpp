//
// Created by Kim HOANG on 22.10.2019.
//

#ifndef TILTON_MACROPROCESSOR_HPP
#define TILTON_MACROPROCESSOR_HPP

#include "Context.hpp"
#include "HashTable.hpp"
#include "Text.hpp"
#include <map>


class MacroProcessor
{
public:
	MacroProcessor();

	virtual ~MacroProcessor();

	void setArgument(int pos, const std::string &value);

	void setMacro(const std::string &name, const std::string &definition);

	// Run
	// Read the standard input and expand the macros, write to standard out
	void Run(bool go);

	void Run(std::istream &input, std::ostream &output);

private:
	void InstallTiltonMacros();

	ContextPtr m_topFrame;
	Text *                        in_;
	Text                            the_output_;
	//std::map<char, OptionProcessor *> option_processors_;
	HashTable						m_macros;
};

#endif // TILTON_MACROPROCESSOR_HPP
