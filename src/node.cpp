// Copyright (c) 2011 Revelux Labs, LLC. All rights reserved.
// Use of this source code is governed by a MIT-style license that can be
// found in the LICENSE file.

#include "node.h"
#include <cassert>
#include <cstdio>

Node::Node(Text *t)
{
	text_  = t;
	value_ = nullptr;
	next_  = nullptr;
}

Node::~Node()
{
	delete this->text_;
	text_ = nullptr;
	delete this->value_;
	value_ = nullptr;

	assert(this != next_);
	delete this->next_;
	next_ = nullptr;
}

void Node::WriteNode()
{
	if (text_)
	{
		fwrite(text_->str().c_str(), sizeof(char), text_->str().length(), stderr);
	}
	if (next_)
	{
		fputc('~', stderr);
		next_->WriteNode();
	}
}
