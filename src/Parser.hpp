//
// Created by Kim on 29.11.19.
//

#ifndef TILTON_PARSER_HPP
#define TILTON_PARSER_HPP

#include <deque>
#include <istream>
#include <string>
#include <stack>
#include "Frame.hpp"
#include "HashTable.hpp"
#include "FrameStack.hpp"

class Parser
{
public:
	Parser() = delete;
	Parser(Parser& other) = delete;
	Parser(std::ostream &output, HashTable &macros, FrameStack &frames);

	~Parser();

	void parse(std::istream &input);

	/**
	Parsers a line
	Returns the number if chars/bytes parsed.
	**/
	unsigned parse(const std::string &line);

private:

	enum class State
	{
		Start = 0,
		MacroStart = 1,
		Tilde = 2,
		MacroEnd = 3
	};

	std::ostream &m_output;
	State m_state = State::Start;
	HashTable &m_macros;
	FrameStack &m_frames;
};


#endif //TILTON_PARSER_HPP
