//
// Created by Kim HOANG <foss@aggsol.de> on 12.12.19.
// Copyright (c) 2019
//

#ifndef TILTON_FRAMESTACK_HPP
#define TILTON_FRAMESTACK_HPP

#include <deque>
#include "Frame.hpp"

class FrameStack
{
public:
	[[nodiscard]]
	Frame &top();

	void push();

	void pop();

	/*
	Returns true if only one Frame is left, false otherwise
	*/
	[[nodiscard]]
	bool isFinal() const { return m_frames.size() == 1; }

	[[nodiscard]]
	unsigned size() const { return m_frames.size(); }

private:
	std::deque<Frame> m_frames;
};


#endif //TILTON_FRAMESTACK_HPP
