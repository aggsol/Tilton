// Copyright (c) 2011 Revelux Labs, LLC. All rights reserved.
// Use of this source code is governed by a MIT-style license that can be
// found in the LICENSE file.

#ifndef SRC_TEXT_H_
#define SRC_TEXT_H_

#include "tilton.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

class Context;
class Macro;

// Text
//  Text wraps a string, and provides methods for setting and modifying the
//  string and for doing I/O with it. A Text can also have a name, which
//  is used as a macro name.
//  The encoding of strings is UTF-8 (the 8-bit form of Unicode). A character
//  is between 1 and 4 bytes in length. The utfLength and utfSubstr methods
//  count multibyte characters. However, if a multibyte character appears to
//  be badly formed, it will interpret the first byte as a single byte
//  character. So while expecting UTF-8 encoded strings, it will usually
//  do the right thing with Latin-1 and similar encodings.

class Text
{
  public:
	Text();

	explicit Text(const std::string &str);
	explicit Text(int len);
	explicit Text(Text *t);
	explicit Text(Macro *t);

	const std::string& str() const { return string_; }
	void str(const std::string& s) { string_ = s; }

	const std::string& name() const {return name_;}
	void name(const std::string& n) { name_ = n; }

	void append(int c);
	void append(int c, int n);
	void append(Text *t);
	void append(const std::string &str);

	// appends a number to the string wrapped by Text
	void appendNum(number);

	// move an index into string_ to the next UTF-8 character
	int AdvanceToNextChar(unsigned index);

	void clear() { string_.clear(); }
	bool empty() const { return string_.empty();}

	// retrieve a character from string
	int GetCharacter(unsigned index) const;

	// retrieve a number from string
	number getNumber();

	//  read the file in 10K chunks and add to string_
	bool ReadFromFile(Text *t);

	void ReadFromStream(std::istream &stream);

	bool IsEqual(Text *t);

	bool lt(Text *t);

	// write string_ to stdout
	void WriteStdOutput();

	Text *RemoveFromString(unsigned index);

	// trims whitespace before appending to string_
	void RemoveSpacesAddToString(Text *t);

	unsigned utfLength() const;

	Text *utfSubstr(int start, int len);

	// writes string_ to a file
	bool WriteToFile(Text *t);

	bool isDigit() const;

	// Tests to see if a string is all digits
	bool allDigits() const;

	// Tests to see if the arg is a digit
	static bool isDigit(int arg_number) { return arg_number >= 0 && arg_number <= 9; }

	// unlike atoi, this function knows about Text
	int ConvertAlphaToInteger();

  private:
	void InitializeText(const char *s, int len);

	// ltNum
	// less than for numbers
	// used by lt
	bool ltNum(Text *t);

	std::string name_;
	std::string string_;
};

#endif // SRC_TEXT_H_
