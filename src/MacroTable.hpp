////
//// Created by Kim HOANG on 22.10.2019.
////
//
//#ifndef TILTON_MACROTABLE_HPP
//#define TILTON_MACROTABLE_HPP
//
//class HashTable;
////TODO: why is this a singelton?
//// MacroTable -- singleton to hold the macro table
//
//class MacroTable
//{
//  public:
//	MacroTable();
//	virtual ~MacroTable();
//
//	static MacroTable *instance();
//
//	// macro_table
//	// Retrieve the macro table
//	HashTable *macro_table() { return macro_table_; }
//
//  private:
//	static MacroTable *pInstance;
//	HashTable *        macro_table_;
//};
//
//#endif // TILTON_MACROTABLE_HPP
//
