// Copyright (c) 2011 Revelux Labs, LLC. All rights reserved.
// Use of this source code is governed by a MIT-style license that can be
// found in the LICENSE file.

#ifndef SRC_MACRO_H_
#define SRC_MACRO_H_

#include "tilton.h"
#include "Context.hpp"
#include "Text.hpp"
#include <string>

class Context;

typedef void (*Builtin)(ContextPtr context, Text& the_output);

// Macro -- represents the name and expansion text of a macro.
class Macro
{
  public:
	Macro();
	explicit Macro(int len);
	explicit Macro(const std::string &def, const std::string& name);
	explicit Macro(Text *t);

	// append
	// appends text to the string wrapped by Macro
	void AddToString(const char *s, int len);
	void AddToString(Text *t);
	void AddToString(const std::string &str);

	// PrintMacroList
	// write the macro list of stdout
	void PrintMacroList();

	// FindFirstSubstring
	// returns the first match in the macro definition
	int FindFirstSubstring(Text *t);

	// IsNameEqual
	bool IsNameEqual(Text *t);

	// find the last occurance of a substring
	int FindLastSubstring(Text *t);

	// set_string
	// setter for m_definition
	void set_string(Text *t);
	void setDef(const std::string& str);

	// name
	// setter for name_
	void set_name(const char *s);

	void set_name(const char *s, int len);

	void set_name(Text *t);

	// ReplaceDefWithSubstring
	// replace the definition with a substring of the definition
	void ReplaceDefWithSubstring(int start, int len);

	std::string m_definition;
	std::string name_;

private:

	void init(const char *s, int len);
};

#endif // SRC_MACRO_H_
