// Copyright (c) 2011 Revelux Labs, LLC. All rights reserved.
// Use of this source code is governed by a MIT-style license that can be
// found in the LICENSE file.

#include "Text.hpp"

#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "Macro.hpp"
#include "tilton.h"
#include <cassert>

Text::Text() { InitializeText(nullptr, 0); }

Text::Text(int len) { InitializeText(nullptr, len); }

Text::Text(const std::string &str) { InitializeText(str.c_str(), str.length()); }

Text::Text(Text *t)
{
	if (t)
	{
		InitializeText(t->string_.c_str(), t->string_.length());
	}
	else
	{
		InitializeText(nullptr, 0);
	}
}

Text::Text(Macro *m)
{
	if (m)
	{
		// copy string
		InitializeText(m->m_definition.c_str(), m->m_definition.length());
		// copy name
		name_ = m->name_;
	}
	else
	{
		InitializeText(nullptr, 0);
	}
}

void Text::append(const std::string &str)
{
	string_ += str;
}

void Text::append(int c)
{
	// TODO?! type trait char
	string_ += static_cast<char>(c);
}

void Text::append(int c, int n)
{
	//    CheckLengthAndIncrease(n);
	while (n > 0)
	{
		string_ += static_cast<char>(c);
		//        string_[length_] = static_cast<char>(c);
		//        length_ += 1;
		n -= 1;
	}
//	my_hash_ = 0;
}

void Text::append(Text *t)
{
	if (t)
	{
		append(t->string_);
	}
}

void Text::appendNum(number n)
{
	number d;
	if (n != kNAN)
	{
		if (n < 0)
		{
			append('-');
			n = -n;
		}
		d = n / 10;
		if (d > 0)
		{
			appendNum(d);
		}
		append(static_cast<int>((n % 10) + '0'));
	}
}

int Text::GetCharacter(unsigned index) const
{
	if (index >= string_.length())
	{
		return EOT;
	}
	return string_.at(index);
}

number Text::getNumber()
{
	int    c;
	unsigned    i     = 0;
	bool   sign  = false;
	bool   ok    = false;
	number value = 0;

	if (string_.length() == 0)
	{
		return kNAN; // jr 31Aug11
	}

	for (;;)
	{
		c = string_.at(i);
		i += 1;
		if (i > string_.length())
		{
			return kNAN;
		}
		if (c > ' ')
		{
			break;
		}
	}
	if (c == '-')
	{
		sign = true;
		c    = string_.at(i);
		i += 1;
		if (i > string_.length())
		{
			return kNAN;
		}
	}
	for (;;)
	{
		if (c >= '0' && c <= '9')
		{
			value = (value * 10) + (c - '0');
			ok    = true;
			if (value < 0)
			{
				ok = false;
				break;
			}
		}
		else
		{
			for (;;)
			{
				if (c > ' ')
				{
					return kNAN;
				}
				if (i >= string_.length())
				{
					break;
				}
				c = string_.at(i);
				i += 1;
			}
		}
		if (i >= string_.length())
		{
			break;
		}
		c = string_.at(i);
		i += 1;
	}
	if (ok)
	{
		if (sign)
		{
			return -value;
		}
		else
		{
			return value;
		}
	}
	else
	{
		return kNAN;
	}
}

void Text::InitializeText(const char *s, int len)
{
	if (s != nullptr)
	{
		string_.assign(s, len);
	}
	else
	{
		string_.reserve(len);
	}
}

bool Text::IsEqual(Text *t)
{
	assert(t != nullptr);
	return t->string_ == this->string_;
}

bool Text::lt(Text *t)
{
	assert(t != nullptr);
	// refactored to seperate all digits from text -- jr 28Aug11
	if (this->allDigits() && t->allDigits())
	{
		return this->ltNum(t);
	}
	else
	{
		return this->string_ < t->string_;
//		return this->ltStr(t);
	}
}

bool Text::ltNum(Text *t)
{
	assert(t != nullptr);
	int first = this->ConvertAlphaToInteger();
	int second = t->ConvertAlphaToInteger();

	return first < second;
}

void Text::WriteStdOutput()
{
	// std::cout << string_;
	fwrite(string_.c_str(), sizeof(char), string_.length(), stdout);
}

bool Text::ReadFromFile(Text *filename)
{
	if (filename == nullptr)
	{
		throw std::runtime_error("Invalid filename");
	}
	std::ifstream file;
	file.open(filename->string_);

	if (not file.is_open())
	{
		throw std::runtime_error(filename->string_ + " Cannot open file");
	}
	ReadFromStream(file);

	return true;
}

Text *Text::RemoveFromString(unsigned index)
{
	if (index < string_.length())
	{
		auto sub = string_.substr(index);

		string_.resize(index, '@');
		return new Text(sub);
	}
	else
	{
		return new Text();
	}
}

// trim is like append, except that it trims leading, trailing spaces, and
// reduces runs of whitespace to single space
void Text::RemoveSpacesAddToString(Text *t)
{
	const char *s = t->string_.c_str();
	int         l = t->string_.length();
	int         i = 0;
	bool        b = false;
	for (;;)
	{
		while (s[i] > ' ')
		{
			append(s[i]);
			b = true;
			i += 1;
			if (i >= l)
			{
				return;
			}
		}
		do
		{
			i += 1;
			if (i >= l)
			{
				return;
			}
		} while (s[i] <= ' ');
		if (b)
		{
			append(' ');
		}
	}
}

// Taken from http://www.zedwood.com/article/cpp-utf8-strlen-function
// Creative Commons CC-By-SA 3.0
unsigned Text::utfLength() const
{
	unsigned c, i, ix, q = 0;
	for(q = 0, i = 0, ix = string_.length(); i < ix; i++, q++)
	{
		c = static_cast<unsigned>(string_.at(i));
		if(c <= 127)
		{
			i += 0;
		}
		else if((c & 0xE0) == 0xC0)
		{
			i += 1;
		}
		else if ((c & 0xF0) == 0xE0)
		{
			i += 2;
		}
		else if ((c & 0xF8) == 0xF0)
		{
			i += 3;
			// else if (($c & 0xFC) == 0xF8) i+=4; // 111110bb //byte 5, unnecessary in 4 byte UTF-8
			// else if (($c & 0xFE) == 0xFC) i+=5; // 1111110b //byte 6, unnecessary in 4 byte UTF-8
		}
		else
		{
			return 0;
		} // invalid utf8
	}
	return q;
}

int Text::AdvanceToNextChar(unsigned i)
{
	int c = string_.at(i) & 0xFF;
	i += 1;
	if (c >= 0xC0)
	{
		if (c < 0xE0)
		{ // 2-byte form
			// <= jr 18Sep11
			if ((i + 1) <= string_.length() && ((string_.at(i) & 0xC0) == 0x80))
			{
				i += 1;
			}
		}
		else if (c < 0xF0)
		{ // 3-byte form
			// <= jr 18Sep11
			if ((i + 2) <= string_.length() && ((string_.at(i) & 0xC0) == 0x80) && ((string_[i + 1] & 0xC0) == 0x80))
			{
				i += 2;
			}
		}
		else
		{ // 4-byte form
			// <= jr 18Sep11
			if ((i + 3) <= string_.length() && ((string_.at(i) & 0xC0) == 0x80) &&
			    ((string_.at(i + 1) & 0xC0) == 0x80) && ((string_.at(i + 2) & 0xC0) == 0x80))
			{
				i += 3;
			}
		}
	}
	return i;
}

Text *Text::utfSubstr(const int start, int len)
{
	unsigned   i = 0;
	int   j;
	Text *t;

	// skip start UTF-8 chars in string
	for (j = 0; j <= start - 1; j++)
	{
		if (i >= string_.length())
		{
			return nullptr;
		}
		i = AdvanceToNextChar(i);
	}

	// make a new string
	t = new Text();

	// and copy UTF-8 chars
	while (len && i < string_.length())
	{
		j = AdvanceToNextChar(i);
		t->append(string_.substr(i, j - i));
//		t->append(&string_.at(i), j - i);
		i = j;
		len -= 1;
	}
	return t;
}

bool Text::WriteToFile(Text *filename)
{
	FILE *fp;
	char  fname[256];
	memmove(fname, filename->string_.c_str(), filename->string_.length());
	fname[filename->string_.length()] = 0;
	fp                                = fopen(fname, "wb");
	if (fp)
	{
		fwrite(string_.c_str(), sizeof(char), string_.length(), fp);
		fclose(fp);
		return true;
	}
	else
	{
		return false;
	}
}

void Text::ReadFromStream(std::istream &stream)
{
	std::ostringstream os;
	os << stream.rdbuf();
	string_ = os.str();
}

int Text::ConvertAlphaToInteger()
{
	try
	{
		return std::stoi(string_);
	}
	catch(const std::invalid_argument &)
	{
		throw std::runtime_error("Cannot convert \"" + string_ + "\" to an integer");
	}
	catch(const std::out_of_range &)
	{
		throw std::runtime_error("Number to large: " + string_);
	}
}

bool Text::isDigit() const
{
	if(string_.length() != 1) return false;
	return std::isdigit(string_[0]);
}

bool Text::allDigits() const
{
	if(string_.empty()) return false;
	for(unsigned i=0; i<string_.length(); ++i)
	{
		if(std::isdigit(string_[i]) == false)
		{
			return false;
		}
	}
	return true;
}
