//
// Created by Kim HOANG <foss@aggsol.de> on 12.12.19.
// Copyright (c) 2019
//

#include "FrameStack.hpp"

Frame &FrameStack::top()
{
	if(m_frames.empty())
	{ throw std::runtime_error("Empty FrameStack"); }
	return m_frames.back();
}

void FrameStack::push()
{
	m_frames.emplace_back();
}

void FrameStack::pop()
{
	m_frames.pop_back();
}
