FROM gitpod/workspace-full

USER root
RUN sudo apt-get update

RUN sudo apt-get install -y \
    lcov \
    graphviz \
    pandoc \
    texlive \
    texlive-xetex \
    texlive-latex-extra \
    texlive-lang-german \
    texlive-fonts-extra \
    gpp \
    ccache \
    cppcheck \
    shelltestrunner \
	zzuf \
	todotxt-cli

RUN sudo apt-get autoremove -y

# Install custom tools, runtime, etc. using apt-get
# For example, the command below would install "bastet" - a command line tetris clone:
#
# RUN apt-get update \
#    && apt-get install -y bastet \
#    && apt-get clean && rm -rf /var/cache/apt/* && rm -rf /var/lib/apt/lists/* && rm -rf /tmp/*
#
# More information: https://www.gitpod.io/docs/42_config_docker/
