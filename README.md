# The Tilton Macro Processor

`<~success~n~macros~>`

## About

Tilton is a simple macro processor. It is small, portable, and Unicode compatible.
It is an even uglier language than Lisp because it replaces the parens with angle
brackets and tildes while lacking any useful data structures.

Macro expressions are completely contained within a set of macro brackets.

`<~ ~>`

Macro arguments are separated by tildes. Adjacency is concatenation.

Tilton only uses those three characters. All other characters are treated
literally, even whitespace. This makes Tilton uncommonly good at dealing with
textual content.

The character set used by Tilton is the UTF-8 form of Unicode. Tilton's character
functions (**length** and **substr**) deal in whole Unicode characters, not in
bytes.

## Easy Usage

In its easiest form, Tilton requires no programming. For example, the shell command

```sh
$ tilton deluxe bogus <base >result
```

will replace all occurrences of `<~1~>` in the file base with deluxe and will
replace all occurrences of `<~2~>` with bogus and store the result in the file
result.

The shell command

```sh
$ tilton -s name "Carl Hollywood"
```

will replace all occurances of `<~name~>` with Carl Hollywood in the standard
input.

Tilton has two functions that can be used to include additional files. These can
be embedded in the input text.

    <~read~filename~>

The read function reads a file and inserts its contents.

    <~include~filename~parameters~>

The include function reads a file, but evaluates it before inserting it. It can
take a set of parameters and do substitutions with them.

These functions can be used together, so that the statement

    <~include~macros<~3~>.tilton~>

will use the third command line argument to select the name of a file to include.
If the third argument is "mac", then the file included will be `macrosmac.tilton`.

## macros
A macro expression is

	<~macroname~firstArgument~secondArgument...~>

If `macroname` matches the name of a known macro, then the macro is expanded with
the arguments, and the macro expression is replaced with its result. Macro
expressions can be nested to any depth.

Extra arguments are usually ignored. Missing arguments are usually treated as
empty text. Some of the built-in macros report errors if the correct number of
parameters is not provided.

Tilton does lazy evaluation. Each parameter of a macro is evaluated zero or one
times. A parameter is not evaluated until its value is needed. That value is
reused if the parameter needs to be evaluated again. This makes it possible to
write functions like min without worrying about side-effects due to multiple
evaluations of the arguments. It is possible to force evaluation of arguments
with the mute function.

There are no rules on macro names. You can call them anything you like, with any
combination of characters. Names are case sensitive.

The terms macros and variables are used interchangeably. The values stored in
them are strings.

Strings of digits can be treated as though they are integers.

The flexibility of naming can be used to simulate arrays.

	<~set~subscript~18~><~set~myArray[<~subscript~>]~42~>

Because subscript has a value of 18, we will assign 42 to a variable
called `myArray[18]`. Note that the brackets are not special in Tilton.

The macro expressions `<~0~>` thru `<~9~>` are used to access parameters.
They can also be used as local variables. `<~1~>` gets the value of the first
argument. `<~1~value~>` sets it to a new value

## Acknowledgements

* Based on Tilton by [Douglas Crockford July-2002](http://www.crockford.com/tilton/tilton.html)
* Forked from Tilton by [JR August-2011](https://github.com/reveluxlabs/Tilton)

## Licenses

* Tilton is MIT licensed
* tiny-unit is beerware
* cliarguments-cpp is beerware
