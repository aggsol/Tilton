//
// Created by Kim HOANG on 23.10.2019.
//

#include "../src/byte_stream.h"
#include "../src/Context.hpp"
#include "../src/MacroProcessor.hpp"
#include "../src/HashTable.hpp"
#include "../src/Text.hpp"
#include "tiny-unit.hpp"
#include <string>

class ContextTest : public tiny::Unit
{
  public:
	ContextTest() : tiny::Unit("ContextTest")
	{
		tiny::Unit::registerTest(&ContextTest::testTilton, "testTilton");
		tiny::Unit::registerTest(&ContextTest::testCtor, "testCtor");
	}

	static void testTilton()
	{
		Text text01;
		text01.name("[unittest]");
		ByteStream bs(&text01);
		HashTable macros;
		macros.setMacro("tilton", "0.7");
		auto ctx = std::make_shared<Context>(&bs, &macros);

		Text  input("<~tilton~>\n");
		Text  output;

		ctx->ParseAndEvaluate(&input, output);
		std::cout << "output.str().length()=" << output.str().length() << "\n";
		TINY_ASSERT_EQUAL(output.str(), "0.7\n");
	}

	static void testCtor()
	{
		Text       	text01;
		ByteStream 	bs(&text01);
		auto ctx01 = std::make_shared<Context>(&bs, nullptr);
		auto ctx02 = std::make_shared<Context>(ctx01);
	}
};

ContextTest contextTest;
