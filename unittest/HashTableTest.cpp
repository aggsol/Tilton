#include "../src/HashTable.hpp"
#include "../src/Macro.hpp"
#include "../src/Text.hpp"
#include "tiny-unit.hpp"
#include <string>

class HashTableTest : public tiny::Unit
{
  public:
	HashTableTest() : tiny::Unit("HashTableTest")
	{
		tiny::Unit::registerTest(&HashTableTest::insertNullIfNotFound, "insertNullIfNotFound");
		tiny::Unit::registerTest(&HashTableTest::insertMacro, "insertMacro");
		tiny::Unit::registerTest(&HashTableTest::setMacro, "setMacro");
		tiny::Unit::registerTest(&HashTableTest::testCtors, "testCtors");
	}

	static void insertNullIfNotFound()
	{
		HashTable ht;
		Text      text01("ひほわれよう"); // Lass uns Spaß haben
		Text      text02("存在する");     // Existieren

		ht.InstallMacro(&text01, &text02);
		auto nil = ht.LookupMacro(&text02);
		TINY_ASSERT_OK(nil == nullptr);
		auto exists = ht.GetMacroDefOrInsertNull(&text02);
		TINY_ASSERT_OK(exists != nullptr);
		auto found = ht.LookupMacro(&text02);
		TINY_ASSERT_OK(found != nullptr);
		TINY_ASSERT_EQUAL(found->m_definition, "");
	}

	static void insertMacro()
	{
		HashTable ht;
		Text      text01("aaa");
		Text      text02("bbb");
		Text      text03("ccc");
		Macro     macro01("ddd", "");
		Macro     macro02(&text03);

		ht.InstallMacro(&text01, &text02);
		auto result = ht.LookupMacro(&text01);
		TINY_ASSERT_OK(result != nullptr);
		TINY_ASSERT_EQUAL(result->name_, "aaa");
		TINY_ASSERT_EQUAL(result->m_definition, "bbb");

		result = ht.LookupMacro(&text02);
		TINY_ASSERT_OK(result == nullptr);

		ht.InstallMacro(&text02, &macro01);
		result = ht.LookupMacro(&text02);
		TINY_ASSERT_OK(result != nullptr);
		TINY_ASSERT_EQUAL(result->m_definition, "ddd");

		ht.InstallMacro(&text03, &macro02);
		result = ht.LookupMacro(&text03);
		TINY_ASSERT_OK(result != nullptr);
		TINY_ASSERT_EQUAL(text03.str(), macro02.m_definition);
		TINY_ASSERT_EQUAL(text03.str(), macro02.name_);
	}

	static void setMacro()
	{
		HashTable ht;
		ht.setMacro("foo", "bar");

		Text text01("foo");
		Text text02("bar");

		auto result = ht.LookupMacro(&text01);
		TINY_ASSERT_OK(result != nullptr);

		result = ht.LookupMacro(&text02);
		TINY_ASSERT_OK(result == nullptr);
	}

	static void testCtors() { HashTable ht; }
};

HashTableTest hashTableTest;
