//
// Created by kim on 02.12.19.
//
#include "../src/HashTable.hpp"
#include "../src/Parser.hpp"
#include "tiny-unit.hpp"
#include <string>

class ParserTest : public tiny::Unit
{
public:
	ParserTest()
			: tiny::Unit("ParserTest")
	{
		registerTest(ParserTest::singleDigits, "singleDigits");
		registerTest(ParserTest::macroInFunction, "macroInFunction");
		registerTest(ParserTest::basicFunction, "basicFunction");
		registerTest(ParserTest::basicTest, "basicTest");
	}

	static void singleDigits()
	{
		FrameStack stack;

		HashTable macros;

		std::stringstream out;
		Parser parser(out, macros, stack);

		std::stringstream data;
		data << "<~0~13~><~1~37~>\n"
			<< "<~0~><~1~>";

		parser.parse(data);

		TINY_ASSERT_EQUAL(out.str(), "1337");
	}

	static void macroInFunction()
	{
		FrameStack stack;

		HashTable macros;
		macros.setMacro("one", "1");
		macros.setMacro("two", "2");
		macros.setMacro("three", "3");
		macros.setMacro("four", "4");
		macros.setMacro("five", "5");
		macros.setMacro("six", "6");
		macros.setMacro("seven", "7");
		macros.setMacro("eight", "8");
		macros.setMacro("nine", "9");

		std::stringstream out;
		Parser parser(out, macros, stack);

		parser.parse("<~add~<~one~>~<~two~>~<~three~>~<~four~>~<~five~>~<~six~>~<~seven~>~<~eight~>~<~nine~>~>");

		TINY_ASSERT_EQUAL(out.str(), "45");
	}

	static void basicFunction()
	{
		FrameStack stack;

		HashTable macros;

		std::stringstream out;
		Parser parser(out, macros, stack);

		parser.parse("<~add~1000~337~>");

		TINY_ASSERT_EQUAL(out.str(), "1337");
	}

	static void basicTest()
	{
		FrameStack stack;

		HashTable macros;
		macros.setMacro("foo", "bar");
		macros.setMacro("bar", "1337");

		{	
			std::stringstream out;
			Parser parser(out, macros, stack);

			parser.parse("<~<~foo~>~>");

			TINY_ASSERT_EQUAL(out.str(), "1337");
		}

		{	
			std::stringstream out;
			Parser parser(out, macros, stack);

			parser.parse("<~bar~><~<~foo~>~>\n");

			TINY_ASSERT_EQUAL(out.str(), "13371337\n");
		}
	}
};

ParserTest parserTest;
