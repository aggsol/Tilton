#include "../src/byte_stream.h"
#include "../src/Text.hpp"
#include "tiny-unit.hpp"
#include <stdexcept>
#include <string>

class ByteStreamTest : public tiny::Unit
{
  public:
	ByteStreamTest() : tiny::Unit("ByteStreamTest")
	{
		tiny::Unit::registerTest(&ByteStreamTest::bsUsage, "bsUsage");
		tiny::Unit::registerTest(&ByteStreamTest::bsCtors, "bsCtors");
	}

	static void bsUsage()
	{
		Text       text01("0123456789");
		ByteStream bs01(&text01);

		TINY_ASSERT_EQUAL(bs01.character(), 0);
		TINY_ASSERT_EQUAL(bs01.index(), 0);
		TINY_ASSERT_EQUAL(bs01.line(), 0);

		TINY_ASSERT_EQUAL(bs01.next(), static_cast<int>('0'));
		TINY_ASSERT_EQUAL(bs01.next(), static_cast<int>('1'));
		TINY_ASSERT_EQUAL(bs01.back(), static_cast<int>('1'));
		TINY_ASSERT_EQUAL(bs01.peek(), static_cast<int>('1'));
		TINY_ASSERT_EQUAL(bs01.next(), static_cast<int>('1'));
		TINY_ASSERT_EQUAL(bs01.next(), static_cast<int>('2'));
	}

	static void bsCtors()
	{
		TINY_ASSERT_TRY()
		ByteStream bs(nullptr);
		TINY_ASSERT_CATCH(std::runtime_error);
	}
};

ByteStreamTest ByteStreamTest;
