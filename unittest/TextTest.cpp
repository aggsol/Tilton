#include "../src/Text.hpp"
#include "tiny-unit.hpp"
#include <sstream>
#include <string>

class TextTest : public tiny::Unit
{
  public:
	TextTest() : tiny::Unit("TextTest")
	{
		tiny::Unit::registerTest(&TextTest::textSubString, "textSubString");
		tiny::Unit::registerTest(&TextTest::textCompare, "textCompare");
		tiny::Unit::registerTest(&TextTest::textConvertAlphaToInteger, "textConvertAlphaToInteger");
		tiny::Unit::registerTest(&TextTest::testRemove, "textRemove");
		tiny::Unit::registerTest(&TextTest::testGetChar, "textGetChar");
		tiny::Unit::registerTest(&TextTest::testStream, "textStream");
		tiny::Unit::registerTest(&TextTest::testUtf8Length, "textUtf8Length");
		tiny::Unit::registerTest(&TextTest::testDigits, "textDigits");
		tiny::Unit::registerTest(&TextTest::testAddToString, "textAddToString");
		tiny::Unit::registerTest(&TextTest::testCtors, "textCtors");
	}

	static void textSubString()
	{
		Text text01("foobar");
		auto result = text01.utfSubstr(2, 3);

		TINY_ASSERT_OK(result != nullptr);
		TINY_ASSERT_EQUAL(result->str(), "oba");

		Text text02("i ♥ u");
		result = text02.utfSubstr(2, 1);

		TINY_ASSERT_OK(result != nullptr);
		TINY_ASSERT_EQUAL(result->str(), "♥");


		Text text03("ሰማይ አይታረስ ንጉሥ አይከሰስ።");
		result = text03.utfSubstr(4, 5);

		TINY_ASSERT_OK(result != nullptr);
		TINY_ASSERT_EQUAL(result->str(), "አይታረስ");

	}

	static void textCompare()
	{
		Text text01("a");
		Text text02("z");
		Text text03("100");
		Text text04("999");
		Text text05("FOO");
		Text text06("BAR");

		TINY_ASSERT_OK(text01.lt(&text02));
		TINY_ASSERT_OK(text03.lt(&text04));
		TINY_ASSERT_OK(not text01.lt(&text03));
		TINY_ASSERT_OK(not text05.lt(&text06));
	}

	static void textConvertAlphaToInteger()
	{
		Text text01("1337");
		TINY_ASSERT_EQUAL(text01.ConvertAlphaToInteger(), 1337);

		Text text02("-1337");
		TINY_ASSERT_EQUAL(text02.ConvertAlphaToInteger(), -1337);
	}

	static void testRemove()
	{
		Text  text01("aaabbbccc");
		Text *text02 = text01.RemoveFromString(3);

		TINY_ASSERT_EQUAL(text02->str(), "bbbccc");
		TINY_ASSERT_EQUAL(text01.str(), "aaa");
	}

	static void testGetChar()
	{
		Text text01("abcdefgh");

		TINY_ASSERT_EQUAL(text01.GetCharacter(0), 'a');
		TINY_ASSERT_EQUAL(text01.GetCharacter(1), 'b');
		TINY_ASSERT_EQUAL(text01.GetCharacter(2), 'c');
		TINY_ASSERT_EQUAL(text01.GetCharacter(3), 'd');
	}

	static void testStream()
	{
		std::stringstream ss;
		ss << "Ḽơᶉëᶆ ȋṕšᶙṁ ḍỡḽǭᵳ ʂǐť ӓṁệẗ, ĉṓɲṩḙċťᶒțûɾ ấɖḯƥĭṩčįɳġ ḝłįʈ, șếᶑ ᶁⱺ ẽḭŭŝḿꝋď ṫĕᶆᶈṓɍ ỉñḉīḑȋᵭṵńť ṷŧ ḹẩḇőꝛế éȶ "
		      "đꝍꞎôꝛȇ ᵯáꞡᶇā ąⱡîɋṹẵ.";

		Text text01;
		text01.ReadFromStream(ss);
		TINY_ASSERT_EQUAL(text01.utfLength(), 123);
	}

	static void testUtf8Length()
	{
		Text text01("foobar");
		TINY_ASSERT_EQUAL(text01.utfLength(), 6);

		Text text02("i ♥ u");
		TINY_ASSERT_EQUAL(text02.utfLength(), 5);

		Text text03("ᚻᛖ ᚳᚹᚫᚦ ᚦᚫᛏ ᚻᛖ ᛒᚢᛞᛖ ᚩᚾ ᚦᚫᛗ ᛚᚪᚾᛞᛖ ᚾᚩᚱᚦᚹᛖᚪᚱᛞᚢᛗ ᚹᛁᚦ ᚦᚪ ᚹᛖᛥᚫ");
		TINY_ASSERT_EQUAL(text03.utfLength(), 56);

		Text *text04 = text03.utfSubstr(2, 4);
		TINY_ASSERT_OK(text04 != nullptr);

		//		text04->WriteStdOutput();
		//		Text text05("ᚳᚹᚫᚦ");
		//		TINY_ASSERT_OK(text05.IsEqual(text04));

		delete text04;
	}

	static void testDigits()
	{
		Text text01("1234567890");
		TINY_ASSERT_OK(text01.allDigits());

		Text text02("123456789O");
		TINY_ASSERT_OK(not text02.allDigits());

		Text text03("asd");
		TINY_ASSERT_OK(not text03.allDigits());
	}

	static void testAddToString()
	{
		Text text01("Hello, ");
		Text text02("Hello, world!");
		Text text03("r");

		text01.append(static_cast<int>('w'));
		text01.append(static_cast<int>('o'), 1);
		text01.append(&text03);
		text01.append("ld!");

		TINY_ASSERT_OK(text01.IsEqual(&text02));
	}

	static void testCtors()
	{
		Text text01;
		// Text text02(nullptr)
		Text text03(text01);
		Text text04(10);
	}
};

TextTest textTest;