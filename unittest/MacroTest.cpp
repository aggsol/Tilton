#include "../src/Macro.hpp"
#include "tiny-unit.hpp"
#include <string>

class MacroTest : public tiny::Unit
{
  public:
	MacroTest() : tiny::Unit("MacroTest")
	{
		tiny::Unit::registerTest(&MacroTest::macroDefReplace, "macroDefReplace");
		tiny::Unit::registerTest(&MacroTest::macroSubStr, "macroSubStr");
		tiny::Unit::registerTest(&MacroTest::macroName, "macroName");
		tiny::Unit::registerTest(&MacroTest::macroAddString, "macroAddString");
		tiny::Unit::registerTest(&MacroTest::macroCtors, "macroCtors");
	}

	static void macroDefReplace()
	{
		Macro macro01("aabbccee", "");
		Macro macro02("aabbccee", "");

		macro01.ReplaceDefWithSubstring(0, 0);
		TINY_ASSERT_EQUAL(macro01.m_definition, "");
		macro02.ReplaceDefWithSubstring(2, 2);
		TINY_ASSERT_EQUAL(macro02.m_definition, "bb");
	}

	static void macroSubStr()
	{
		Macro macro01("foobarfizzbarfoo", "");
		Text  foo("foo");
		Text  bar("bar");
		Text  fizz("fizz");

		TINY_ASSERT_EQUAL(macro01.FindFirstSubstring(&foo), 0);
		TINY_ASSERT_EQUAL(macro01.FindFirstSubstring(&bar), 3);
		TINY_ASSERT_EQUAL(macro01.FindFirstSubstring(&fizz), 6);

		TINY_ASSERT_EQUAL(macro01.FindLastSubstring(&foo), 13);
		TINY_ASSERT_EQUAL(macro01.FindLastSubstring(&bar), 10);
		TINY_ASSERT_EQUAL(macro01.FindLastSubstring(&fizz), 6);
	}

	static void macroName()
	{
		Macro macro01(10);
		Text  text01("Hello");
		Text  text02("World");

		macro01.set_string(&text01);
		macro01.set_name("World");
		TINY_ASSERT_OK(macro01.IsNameEqual(&text02));
	}

	static void macroAddString()
	{
		Macro macro01;
		macro01.AddToString("abcd", 3);

		Text text01("defg");
		macro01.AddToString(&text01);

		int pos = macro01.FindFirstSubstring(&text01);
		TINY_ASSERT_EQUAL(pos, 3);

		macro01.AddToString(&text01);
		pos = macro01.FindLastSubstring(&text01);

		TINY_ASSERT_EQUAL(pos, 7);
	}

	static void macroCtors()
	{
		Macro macro01;
		Macro macro02(2);
		Macro macro03("foobar", "");
		Text  text01;
		Macro macro04(&text01);
	}
};

MacroTest macroTest;
