//
// Created by Kim HOANG on 23.10.2019.
//

#include "../src/Context.hpp"
#include "../src/MacroProcessor.hpp"
#include "../src/HashTable.hpp"
#include "../src/Text.hpp"
#include "../src/function.h"
#include "tiny-unit.hpp"
#include <string>

/***
 * The tests here a just named by the tool and iteration number. They were found by
 * fuzztesting nad are kept here to prevent regressions.
 */
class RadamsaFindings : public tiny::Unit
{
public:
	RadamsaFindings() : tiny::Unit("RadamsaFindings")
	{
		//tiny::Unit::registerTest(&RadamsaFindings::radamsa12, "radamsa12");
		tiny::Unit::registerTest(&RadamsaFindings::radamsa6366, "radamsa6366");
		tiny::Unit::registerTest(&RadamsaFindings::radamsa643, "radamsa643");
		tiny::Unit::registerTest(&RadamsaFindings::radamsa35315, "radamsa35315");
		tiny::Unit::registerTest(&RadamsaFindings::radamsa11336, "radamsa11336");
		tiny::Unit::registerTest(&RadamsaFindings::radamsa31, "radamsa31");
	}

	static void radamsa12()
	{
		MacroProcessor macProc;
		

		std::stringstream input;
		input << "󠀶<~define~Greeter~Hi, <~255~> <~7501806~>.~>\n"
			<< "<~eval~<~Greeter~Carl~Hollywood~>~>\n"
		   	<< "<~eval~<~Greeter~Carl~Hollywood~>~>\n";

		std::ostringstream output;

		TINY_ASSERT_TRY()
				macProc.Run(input, output);
		TINY_ASSERT_CATCH(std::runtime_error);
	}

	static void radamsa6366()
	{
		MacroProcessor macProc;
		

		std::stringstream input;
		input << "󠀶<~define~fizz~I exist!~>\n<~delete~fizz~>\n<~define~fizz~I exist!~>\n"
			<< "<~delete~fizz~>\n<~defined?~>\n";
		std::ostringstream output;

		TINY_ASSERT_TRY()
				macProc.Run(input, output);
		TINY_ASSERT_CATCH(std::runtime_error);
	}

	static void radamsa643()
	{
		MacroProcessor macProc;
		

		std::stringstream input;
		input << "󠀶<~define~fizz~I exist!~>\n"
			<< "<~delete~fizz~>\n"
			<< "<~delete~fizz~>\n"
			<< "<~define~fizz~>\n"
			<< "<~delete~fizz~>\n"
			<< "<~defined?~fizz~definedefin�edefin elete~fizz~>\n"
			<< "<~delete~fizz~>\n"
			<< "<~delete~fizz~>\n"
			<< "<~defined?~f󠀸izz~>\n"
			<< "<~defined?~fizz~>\n"
			<< "<~defined?~fizz~>\n"
			<< "<~define~>\n";
		std::ostringstream output;

		TINY_ASSERT_TRY()
				macProc.Run(input, output);
		TINY_ASSERT_CATCH(std::runtime_error);
	}

	static void radamsa35315()
	{
		MacroProcessor macProc;
		

		std::stringstream input;
		input << "󠀶<~9223372036854775811et~name^Tilton~>\n"
			  << "Hi, my name is <~name~>."
			  << "Hi\f my name is <~name~>.";
		std::ostringstream output;

		TINY_ASSERT_TRY()
				macProc.Run(input, output);
		TINY_ASSERT_CATCH(std::runtime_error);

	}

	static void radamsa11336()
	{
		MacroProcessor macProc;
		


		std::stringstream input;
		input << "󠀶<~9223372036854775811et~name^Tilton~>\n"
			  << "Hi, my name is <~name~>."
			  << "Hi\f my name is <~name~>.";
		std::ostringstream output;

		TINY_ASSERT_TRY()
				macProc.Run(input, output);
		TINY_ASSERT_CATCH(std::runtime_error);

	}

	static void radamsa31()
	{
		MacroProcessor macProc;
		

		std::stringstream input;
		input << "󠀶<~set~>.\n";
		std::ostringstream output;

		TINY_ASSERT_TRY()
				macProc.Run(input, output);
		TINY_ASSERT_CATCH(std::runtime_error);
	}
};

RadamsaFindings radamsaFindings;
