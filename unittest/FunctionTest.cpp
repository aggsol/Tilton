#include "tiny-unit.hpp"
#include "../src/FunctionCollection.hpp"
#include <string>
#include <functional>

class FunctionTest : public tiny::Unit
{
  public:
	FunctionTest() : tiny::Unit("FunctionTest")
	{
		tiny::Unit::registerTest(FunctionTest::basicFunctions, "basicFunctions");
	}

	static int half(int v)
	{
		return v / 2;
	}

	static void basicFunctions()
	{
		FunctionCollection<std::function<int(int)>> funColl;

		funColl.add("double", [](int x){ return x * 2; });
		funColl.add("half", &half);

		TINY_ASSERT_EQUAL(funColl.get("double")(2), 4);
		TINY_ASSERT_EQUAL(funColl.get("double")(8), 16);

		TINY_ASSERT_EQUAL(funColl.get("half")(2), 1);
		TINY_ASSERT_EQUAL(funColl.get("half")(8), 4);

		auto fun = funColl.find("foobar");
		TINY_ASSERT_OK(fun == nullptr);

		fun = funColl.find("double");
		TINY_ASSERT_OK(fun != nullptr);
		TINY_ASSERT_EQUAL((*fun)(2), 4);
		TINY_ASSERT_EQUAL((*fun)(8), 16);
	}

};

FunctionTest functionTest;

