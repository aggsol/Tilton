//
// Created by Kim HOANG on 23.10.2019.
//

#include "../src/MacroProcessor.hpp"
#include "../src/function.h"
#include "tiny-unit.hpp"

class MacroProcessorTest : public tiny::Unit
{
  public:
	MacroProcessorTest() : tiny::Unit("MacroProcessorTest")
	{
		//tiny::Unit::registerTest(&MacroProcessorTest::basicLoop, "basicLoop");
		tiny::Unit::registerTest(&MacroProcessorTest::evalGreeter, "evalGreeter");
		tiny::Unit::registerTest(&MacroProcessorTest::arithmetic, "arithmetic");
		tiny::Unit::registerTest(&MacroProcessorTest::add, "add");
		tiny::Unit::registerTest(&MacroProcessorTest::lastDelimiter, "lastDelimiter");
		tiny::Unit::registerTest(&MacroProcessorTest::definedDeleted, "definedDeleted");
		tiny::Unit::registerTest(&MacroProcessorTest::mpVersion, "mpVersion");
		tiny::Unit::registerTest(&MacroProcessorTest::testCtor, "testCtor");
	}

	static void evalGreeter()
	{
		MacroProcessor macProc;

		std::stringstream input;
		input << "<~define~Greeter~Hi, <~1~> <~2~>.~>\n"
			  << "<~eval~<~Greeter~Carl~Hollywood~>~>\n";
		std::ostringstream output;

		macProc.Run(input, output);

		TINY_ASSERT_EQUAL(output.str(), "\nHi, Carl Hollywood.\n");
	}

	static void add()
	{
		MacroProcessor macProc;
		macProc.setMacro("a", "1");
		macProc.setMacro("b", "2");
		macProc.setMacro("c", "3");
		macProc.setMacro("d", "5");

		std::stringstream input;
		input << "<~add~<~a~>~<~b~>~<~c~>~<~d~>~>";
		std::ostringstream output;

		macProc.Run(input, output);

		TINY_ASSERT_EQUAL(output.str(), "11");
	}

	static void arithmetic()
	{
		MacroProcessor macProc;
		macProc.setMacro("a", "16");
		macProc.setMacro("b", "8");

		std::stringstream input;
		input << "<~add~<~a~>~<~b~>~>\n"
			  << "<~div~<~a~>~<~b~>~>\n"
		<< "<~sub~<~a~>~<~b~>~>\n"
		<< "<~mul~<~a~>~<~b~>~>\n";
		std::ostringstream output;

		macProc.Run(input, output);

		TINY_ASSERT_EQUAL(output.str(), "24\n2\n8\n128\n");
	}

	static void basicLoop()
	{
		MacroProcessor macProc;
		std::stringstream input;
		input << "<~set~counter~<~1~>~>\n"
				 "<~loop~<~counter~>~<~counter~><~set~counter~<~sub~<~counter~>~1~>~>~>\n";
		std::ostringstream output;

		macProc.Run(input, output);

		TINY_ASSERT_EQUAL(output.str(), "\nc\n;\n");
	}

	static  void lastDelimiter()
	{
		MacroProcessor macProc;
		std::stringstream input;
		input << "<~set~input~a;b;c~>\n<~last~input~;~>\n<~0~>\n";
		std::ostringstream output;

		macProc.Run(input, output);

		TINY_ASSERT_EQUAL(output.str(), "\nc\n;\n");
	}

	static void definedDeleted()
	{
		MacroProcessor macProc;
		std::stringstream input;
		input << "<~set~fizz~I exist!~>"
		      << "<~delete~fizz~>"
		      << "<~defined?~fizz~defined~notDefined~>";
		std::ostringstream output;

		macProc.Run(input, output);

		TINY_ASSERT_EQUAL(output.str(), "notDefined");
	}

	static void mpVersion()
	{
		MacroProcessor macProc;
		std::stringstream input;
		input << "<~tilton~>\n";
		std::ostringstream output;

		macProc.Run(input, output);

		TINY_ASSERT_EQUAL(output.str(), "0.7\n");
	}

	static void testCtor() { MacroProcessor macProc; }
};

MacroProcessorTest macroProcessorTest;
