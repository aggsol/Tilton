# How To Build

Tilton provides easy building with CMake. Just enter your clones repository and type

```sh
$ mkdir build
$ cd build
$ cmake ..
$ cmake --build .
```

## Build Debian Package

**TODO**

## Build Tests

Unit test are build with the test target. To execute it in the build folder

```sh
$ ./test
```

### Command Line Interface Tests

The folder `clitest` contains test cases for the [shelltestrunner](https://github.com/simonmichael/shelltestrunner)
that execute the just build Titlton. After a success build type

```sh
$ cd ../clitest
$ shelltest -pac *.test
```

## Build Code Coverage

**TODO**